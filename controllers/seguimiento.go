package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarSeguimiento(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	seguimiento := models.Seguimiento{}
	err := json.NewDecoder(r.Body).Decode(&seguimiento)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}

	b := models.CrearSeguimiento(
		seguimiento.VozCliente,
		seguimiento.AlertaTDP,
		seguimiento.ResultadoAlerta,
		seguimiento.AtencionAlerta,
		seguimiento.DetalleAtencion,
		seguimiento.FechaCitaRetorno,
		seguimiento.TipoDescuento,
		seguimiento.Caducidad,
		seguimiento.TipoServicio,
		seguimiento.Servicio,
		seguimiento.Otro)
	fmt.Println(b)
	models.Closeconnection()
}

func ListarSeguimientos(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	seguimientol := models.ListarSeguimientos()
	j, err := json.Marshal(seguimientol)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarSeguimiento(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	idSeguimiento := vars["idSeguimiento"] //Esto ya es un String
	res := models.ListarSeguimiento(idSeguimiento)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
