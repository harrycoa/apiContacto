package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
	"github.com/rafalgolarz/timestamp"
)

// SelectContacto controlador de prueba 3
func InsertarContacto(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	contacto := models.Contacto{}
	err := json.NewDecoder(r.Body).Decode(&contacto)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	a := models.CrearContacto(contacto.TrabajadorCorasur,
		contacto.TipoContactoCorasur,
		contacto.ContactoCorasur,
		contacto.Placa,
		contacto.IdVehiculo,
		contacto.Cliente,
		contacto.TipoContactoCliente,
		contacto.ContactoCliente,
		contacto.Activo,
		contacto.ResultadoContacto,
		contacto.FechaRetomarContacto,
		contacto.UltimoKilometrajeServicio,
		timestamp.FromNow{}.String(),
		contacto.IdMotivo,
		contacto.IdEstado,
		contacto.IdRespuesta,
		contacto.ManttoK,
		contacto.NuevoDuenio,
		contacto.TelefonoNuevoDuenio,
		contacto.AgendaCita,
		contacto.EstadoContacto,
		contacto.VozCliente,
		contacto.FechaCitaReferencial1,
		contacto.DocumentoRecojo,
		contacto.FechaRecojoDocumento,
		contacto.Otro,
		contacto.IdNoCita,
		contacto.IdPresupuesto,
		contacto.IdOrdenTrabajo,
		contacto.IdPostergacion,
		contacto.IdEncuesta,
		contacto.IdSeguimiento,
		contacto.IdRecomendacion,
		contacto.MotivoDetalle,
		contacto.MotivoDetalleDescripcion)
	fmt.Println(a)
	models.Closeconnection()
}

func ListarContacto(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	contactol := models.ListarContactos()
	j, err := json.Marshal(contactol)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()

}

func ActualizarContacto(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	contacto := models.Contacto{}
	err := json.NewDecoder(r.Body).Decode(&contacto)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	a := models.CrearContacto(contacto.TrabajadorCorasur,
		contacto.TipoContactoCorasur,
		contacto.ContactoCorasur,
		contacto.Placa,
		contacto.IdVehiculo,
		contacto.Cliente,
		contacto.TipoContactoCliente,
		contacto.ContactoCliente,
		contacto.Activo,
		contacto.ResultadoContacto,
		contacto.FechaRetomarContacto,
		contacto.UltimoKilometrajeServicio,
		timestamp.FromNow{}.String(),
		contacto.IdMotivo,
		contacto.IdEstado,
		contacto.IdRespuesta,
		contacto.ManttoK,
		contacto.NuevoDuenio,
		contacto.TelefonoNuevoDuenio,
		contacto.AgendaCita,
		contacto.EstadoContacto,
		contacto.VozCliente,
		contacto.FechaCitaReferencial1,
		contacto.DocumentoRecojo,
		contacto.FechaRecojoDocumento,
		contacto.Otro,
		contacto.IdNoCita,
		contacto.IdPresupuesto,
		contacto.IdOrdenTrabajo,
		contacto.IdPostergacion,
		contacto.IdEncuesta,
		contacto.IdSeguimiento,
		contacto.IdRecomendacion,
		contacto.MotivoDetalle,
		contacto.MotivoDetalleDescripcion)
	fmt.Println(a)
	models.Closeconnection()
}

func EliminarContacto(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	defer r.Body.Close()
	contacto := models.Contacto{}
	if err := json.NewDecoder(r.Body).Decode(&contacto); err != nil {
		contacto.Delete()
	}
	models.Closeconnection()
}

func ListarContactoParametro(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	placa := vars["placa"] //Esto ya es un String
	res := models.ListarContactosParametro(placa)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
