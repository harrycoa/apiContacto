package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"../models"
	"github.com/gorilla/mux"
)

// SelectOrdenReparacion controlador de prueba 3
func ListarOrden(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	placa := vars["placa"] //Esto ya es un String
	//fmt.Fprintf(w, placa)
	//fmt.Println(placa)
	res := models.ListarOrdenes(placa)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()

}
