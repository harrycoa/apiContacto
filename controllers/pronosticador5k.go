package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarPronostico5k(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	pronostico5k := models.Pronostico5k{}
	err := json.NewDecoder(r.Body).Decode(&pronostico5k)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}

	a := models.CrearPronostico5k(
		pronostico5k.Placa,
		pronostico5k.Modelo,
		pronostico5k.Cliente,
		pronostico5k.Telefono1,
		pronostico5k.Telefono2,
		pronostico5k.Mantenimiento,
		pronostico5k.Prioridad,
		// new
		pronostico5k.KmPenultimoServicio,
		pronostico5k.KmUltimoServicio,
		pronostico5k.KmRestoUP,
		pronostico5k.ManttoToca,
		pronostico5k.FechaPenultimoServicio,
		pronostico5k.FechaUltimoServicio,
		pronostico5k.RestoFechasUP,
		pronostico5k.KmPromedioFechaServicio,
		pronostico5k.FaltaManttoToca,
		pronostico5k.CantDiasParaMantto,
		pronostico5k.Pronostico,
		// det
		pronostico5k.Fecha15antes,
		pronostico5k.FechaPactadaUltimoContacto,
		pronostico5k.FechaCorregida,
		pronostico5k.FechaCorregidaSistema,
		pronostico5k.FechaWppEnvio,
		pronostico5k.Vistobueno,
		pronostico5k.Resultado,
		pronostico5k.FechaLlamadaEfectuada,
		pronostico5k.Contesta,
		pronostico5k.Resultado2,
		pronostico5k.FechaPactadaVolverLlamar,
		pronostico5k.FechaCita,
		pronostico5k.Recordatorio,
		pronostico5k.FechaRegistro,
		pronostico5k.NroPresupuesto,
		pronostico5k.Recomendacion,
		pronostico5k.Costo)
	fmt.Println(a)
	models.Closeconnection()
}

func ActualizarPronostico5k(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	pronostico5k := models.Pronostico5k{}
	err := json.NewDecoder(r.Body).Decode(&pronostico5k)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}

	a := models.CrearPronostico5k(
		pronostico5k.Placa,
		pronostico5k.Modelo,
		pronostico5k.Cliente,
		pronostico5k.Telefono1,
		pronostico5k.Telefono2,
		pronostico5k.Mantenimiento,
		pronostico5k.Prioridad,
		// new
		pronostico5k.KmPenultimoServicio,
		pronostico5k.KmUltimoServicio,
		pronostico5k.KmRestoUP,
		pronostico5k.ManttoToca,
		pronostico5k.FechaPenultimoServicio,
		pronostico5k.FechaUltimoServicio,
		pronostico5k.RestoFechasUP,
		pronostico5k.KmPromedioFechaServicio,
		pronostico5k.FaltaManttoToca,
		pronostico5k.CantDiasParaMantto,
		pronostico5k.Pronostico,
		// det
		pronostico5k.Fecha15antes,
		pronostico5k.FechaPactadaUltimoContacto,
		pronostico5k.FechaCorregida,
		pronostico5k.FechaCorregidaSistema,
		pronostico5k.FechaWppEnvio,
		pronostico5k.Vistobueno,
		pronostico5k.Resultado,
		pronostico5k.FechaLlamadaEfectuada,
		pronostico5k.Contesta,
		pronostico5k.Resultado2,
		pronostico5k.FechaPactadaVolverLlamar,
		pronostico5k.FechaCita,
		pronostico5k.Recordatorio,
		pronostico5k.FechaRegistro,
		pronostico5k.NroPresupuesto,
		pronostico5k.Recomendacion,
		pronostico5k.Costo)
	fmt.Println(a)
	models.Closeconnection()
}

func ListarPronostico5k(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	pronostico5kl := models.Pronostico5kLista()
	j, err := json.Marshal(pronostico5kl)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarPronostico5kParametro(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	placa := vars["placa"] //Esto ya es un String
	res := models.Pronostico5kListaParametro(placa)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
