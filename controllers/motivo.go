package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"../models"
	"github.com/gorilla/mux"
)

// SelectMotivo controlador de prueba 3
func ListarMotivos(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	// insertar
	// actualizar
	// listar
	fmt.Println(r.URL.Path)
	s := r.URL.Path
	primera := s[0:1]
	ultima := s[len(s)-1:]
	fmt.Println(primera)
	fmt.Println(ultima)
	motivo := models.ListarMotivos()
	j, err := json.Marshal(motivo)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()

}
func ListarMotivoParametro(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	mclase := vars["mclase"] //Esto ya es un String
	//fmt.Fprintf(w, mclase)
	//fmt.Println(mclase)
	res := models.ListarMotivosParametro(mclase)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
