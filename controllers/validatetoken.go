package controllers

import (
	"context"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"

	"../commons"
	"../models"
	"../models/auth"
)

// ValidateToken validar el token del cliente
func ValidateToken(
	w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	var m models.Message
	token, err := request.ParseFromRequestWithClaims(
		r,
		request.OAuth2Extractor,
		&auth.Claim{},
		func(t *jwt.Token) (interface{}, error) {
			return commons.PublicKey, nil
		},
	)
	if err != nil {

		m.Code = http.StatusUnauthorized
		switch err.(type) {
		case *jwt.ValidationError:

			vError := err.(*jwt.ValidationError)

			switch vError.Errors {

			case jwt.ValidationErrorExpired:
				m.Message = "Su token ha expirado"
				commons.DisplayMessage(w, m)
				return
			case jwt.ValidationErrorSignatureInvalid:
				m.Message = "La firma del token no es válida"
				commons.DisplayMessage(w, m)
				return
			default:
				m.Message = "Token no válido"
				commons.DisplayMessage(w, m)
				return
			}

		default:
			m.Message = "Autenticación Requerida."
			commons.DisplayMessage(w, m)
			return
		}
	}

	if token.Valid {
		ctx := context.WithValue(r.Context(), r.Context().Value("user"), token.Claims.(*auth.Claim).TokenUser)
		next(w, r.WithContext(ctx))
	} else {
		m.Code = http.StatusUnauthorized
		m.Message = "Token no válido"
		commons.DisplayMessage(w, m)
	}
}
