package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/rafalgolarz/timestamp"
)

// SelectContacto controlador de prueba 3
func InsertarGestionNegativa(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	gestion := models.GestionNegativa{}
	err := json.NewDecoder(r.Body).Decode(&gestion)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}

	a := models.CrearGestionNegativa(gestion.Placa,
		gestion.Cliente,
		gestion.ContactoCliente,
		gestion.Activo,
		gestion.IdEstado,
		gestion.ResultadoContacto,
		gestion.FechaRetomarContacto,
		gestion.AtendidoGestion,
		gestion.IndicacionSugerida,
		gestion.DetalleGestion,
		gestion.FechaRetornoCorasur,
		gestion.TipoDescuento,
		gestion.Caducidad,
		gestion.TipoServicio,
		gestion.Servicio,
		timestamp.FromNow{}.String(),
		gestion.IdContacto)
	fmt.Println(a)
	m := models.Message{Code: 201, Message: "Registro Exitoso!"}
	j, err := json.Marshal(m)
	if err != nil {
		log.Fatalf("Error al convertir el mensaje: %s", err)
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(j)
	models.Closeconnection()
}
