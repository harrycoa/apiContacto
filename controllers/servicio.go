package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarServicio(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	servicio := models.Servicio{}
	err := json.NewDecoder(r.Body).Decode(&servicio)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	a := models.CrearServicio(
		servicio.TipoServicio,
		servicio.ClaseServicio,
		servicio.Servicio,
		servicio.DetalleServicio,
		servicio.KmMantenimiento)
	fmt.Println(a)
	models.Closeconnection()
}

func ListarServicios(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	serviciol := models.ListarServicios()
	j, err := json.Marshal(serviciol)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarServicio(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	claseServicio := vars["claseServicio"] //Esto ya es un String
	res := models.ListarServicio(claseServicio)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
