package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarRecomendacion(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	recomendacion := models.Recomendacion{}
	err := json.NewDecoder(r.Body).Decode(&recomendacion)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	b := models.CrearRecomendacion(
		recomendacion.EstadoRecomendacion,
		recomendacion.EstadoPastillaA,
		recomendacion.EstadoPastillaB,
		recomendacion.EstadoPastillaC,
		recomendacion.EstadoPastillaD,
		recomendacion.RepuestoGeneral,
		recomendacion.RepuestoEspecifico,
		recomendacion.RepuestoAlternativo,
		recomendacion.TiempoReparacion,
		recomendacion.BreveRecomendacion,
		recomendacion.Presupuesto,
		recomendacion.HoraInicioRecepcion,
		recomendacion.HoraFinRecepcion,
		recomendacion.HoraInicioLavado,
		recomendacion.HoraFinLavado,
		recomendacion.HoraInicioMantto,
		recomendacion.HoraFinMantto,
		recomendacion.HoraInicioEntrega,
		recomendacion.HoraFinEntrega,
		recomendacion.FechaRegistro,
		recomendacion.HoraRegistro,
		recomendacion.IdPresupuesto)
	fmt.Println(b)
	models.Closeconnection()
}

func ListarRecomendaciones(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	seguimientol := models.ListarRecomendaciones()
	j, err := json.Marshal(seguimientol)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarRecomendacion(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	idRecomendacion := vars["idRecomendacion"] //Esto ya es un String
	res := models.ListarRecomendacion(idRecomendacion)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
