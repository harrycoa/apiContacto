package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"../models"
)

// SelectEstado controlador de prueba 3
func InsertarCita(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	citai := models.Cita{}
	err := json.NewDecoder(r.Body).Decode(&citai)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	a := models.CrearCita(
		citai.Servicio,
		citai.DetalleServicio,
		citai.FechaCita,
		citai.FechaPactadaCita,
		citai.MotivoCita,
		citai.Detallecita,
		citai.ObservacionCita,
		citai.AclaracionRecordatorio,
		citai.FechaCitaCambiada,
		citai.HoraCitaCambiada,
		citai.PersonaCambioCita,
		citai.RazonCambioCita,
		citai.ObservacionCambio,
		citai.MensajeContactoCita,
		citai.FechaNuevoContacto,
		citai.PersonaCancelaCita,
		citai.MotivoCancelaCita,
		citai.FechaRetomarContactoCita,
		citai.VozCliente,
		citai.HoraPosibleArribo,
		citai.EstadoReprogramar,
		citai.HoraInicio,
		citai.HoraEntrega,
		citai.FechaCitaReprogramada,
		citai.ObservacionReprogramacion,
		citai.DetalleIncumplimiento,
		citai.EstadoCita,
		citai.IdServicio,
		citai.IdContacto,
		citai.IdMotivoResultado)
	fmt.Println(a)
	models.Closeconnection()
}

/*
func ListarCita(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	citas := models.ListarCitas()
	j, err := json.Marshal(citas)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()

}

func ActualizarCita(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	cita := models.Cita{}
	err := json.NewDecoder(r.Body).Decode(&cita)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	a := models.CrearCita(cita.FechaCita,
		cita.ClaseCita,
		cita.Detallecita,
		cita.ObservacionCita,
		cita.IdServicioCita)
	fmt.Println(a)
	models.Closeconnection()
}

func EliminarCita(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	defer r.Body.Close()
	cita := models.Cita{}
	if err := json.NewDecoder(r.Body).Decode(&cita); err != nil {
		cita.Delete()
	}
	models.Closeconnection()
}

func ListarCitaParametro(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	claseCita := vars["claseCita"] //Esto ya es un String
	res := models.ListarCitasParametro(claseCita)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()

}
*/
