package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"time"

	cDb "../configuration"
	"../models"
	"../results"
	"github.com/gorilla/mux"
)

// PruebasSp pruebas
func PruebasSp(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	placa := vars["placa"]
	// c := cDb.CredentialsDb()
	// server := fmt.Sprintf("%s:%s", c.Server, c.Port)
	// db := mysql.New("tcp", "", server, c.User, c.Password, c.Database)
	// checkErr(db.Connect())
	// defer db.Close()
	// sp, err := db.Prepare("CALL spPrueba(?,?,?)")
	// checkErr(err)

	// rows, res, err := sp.Exec(1, 2, 3)
	// checkErr(err)
	// fmt.Println("===================================")
	// fmt.Println(rows)
	// fmt.Println("===================================")
	// fmt.Println(res)
	// fmt.Println("===================================")

	// type Data struct {
	// 	a string
	// 	b string
	// 	c string
	// }

	// columna1 := res.Map("columna1")
	// for _, row := range rows {
	// 	obj := new(Data) // Data{}
	// 	obj.a = row.Str(columna1)
	// 	obj.b = row.Str(1)
	// 	obj.c = row.Str(2)
	// 	fmt.Println("===================================")
	// 	fmt.Println(obj)
	// 	fmt.Println("===================================")
	// }

	/*************************************************************************************************************/

	rows, err := cDb.ExecQuerySp("CALL spPronostico5k(?)", placa)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	ordenes := models.OrdenReparaciones{}

	for _, row := range rows {
		o := models.OrdenReparacion{}
		o.Placa = row.Str(0)
		o.Numero = row.Int64(1)
		o.Nombre = row.Str(2)
		o.Km = row.Int64(3)
		o.Telefono = row.Str(4)
		o.Servicio = row.Str(5)
		o.FechaInicio = row.Str(6)
		o.FechaFin = row.Str(7)
		o.Serie = row.Str(8)
		o.Motor = row.Str(9)
		o.Chasis = row.Str(10)
		o.Vin = row.Str(11)
		o.AnioFabricacion = row.Str(12)

		ordenes = append(ordenes, o)
	}
	// m := models.Message{Code: 200, Message: "Ok"}
	j, err := json.Marshal(ordenes)
	if err != nil {
		log.Fatalf("Error al convertir el mensaje: %s", err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)

}

// Pruebas pruebas
func Pruebas(w http.ResponseWriter, r *http.Request) {

	models.CreateConnection()

	ordenes := models.OrdenesPronostico("X4H751")

	// ultimaOrden := ordenes[0]
	// // penultimaOrden := ordenes[1]

	// // f1, _ := time.Parse(time.RFC3339, penultimaOrden.FechaInicio)
	// // fmt.Println(f1)
	// // f2, _ := time.Parse(time.RFC3339, ultimaOrden.FechaInicio)
	// // fmt.Println(f2)

	// // diff := f2.Sub(f1)

	// // fmt.Println(diff.Hours())
	// // fmt.Println(int(diff.Hours() / 24))

	// // kilometraje que lo toca
	// mattoQueLeToca := math.Trunc((float64(ultimaOrden.Km)+5000)/5000) * 5000
	// fmt.Println(ultimaOrden.Km)
	// fmt.Println(mattoQueLeToca)

	//************************************************************
	ultimaOrden := ordenes[0]
	penultimaOrden := ordenes[1]
	km := ultimaOrden.Km - penultimaOrden.Km
	kmpenultserv := ultimaOrden.Km

	fmt.Println(km)

	// numero de días
	f1, _ := time.Parse(time.RFC3339, penultimaOrden.FechaInicio)
	f2, _ := time.Parse(time.RFC3339, ultimaOrden.FechaInicio)
	diff := f2.Sub(f1)
	numeroDias := int64(diff.Hours() / 24)

	// // kilometraje que lo toca
	mattoQueLeToca := math.Trunc((float64(ultimaOrden.Km)+5000)/5000) * 5000

	promedioRecorridoHastaelDia := float64(km) / float64(numeroDias)

	loqueFaltaparaManto := int64(mattoQueLeToca - float64(kmpenultserv))

	cantDiasparamanto := float64(loqueFaltaparaManto) / promedioRecorridoHastaelDia

	fechaError := f2.AddDate(0, 0, int(cantDiasparamanto)).String()
	pronostic := fechaError[0:19]

	fmt.Println(pronostic)

	models.Closeconnection()
	m := models.Message{Code: 200, Message: "Ok"}

	j, err := json.Marshal(m)
	if err != nil {
		log.Fatalf("Error al convertir el mensaje: %s", err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)

}
