package controllers

import (
	"encoding/json"

	"log"
	"net/http"

	"fmt"

	"../models"
	"github.com/gorilla/mux"
)

// SelectContacto controlador de prueba 3
func InsertarOrdenTrabajo(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	obj := models.OrdenTrabajo{}
	err := json.NewDecoder(r.Body).Decode(&obj)
	if err != nil {
		m := models.Message{Code: 1, Message: err.Error()}
		j, err := json.Marshal(m)
		if err != nil {
			log.Fatalf("Error al convertir el mensaje: %s", err)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}
	b := models.CrearOrdenTrabajo(
		obj.FechaRecojo,
		obj.Mensaje,
		obj.FechaSolicitada,
		obj.MotivoPostergacion,
		obj.HoraSolicitada,
		obj.IdMotivoResultado)
	fmt.Println(b)
	models.Closeconnection()
}

func ListarOrdenTrabajos(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	ordentrabajol := models.ListarOrdenTrabajos()
	j, err := json.Marshal(ordentrabajol)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarOrdenTrabajo(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	idorden := vars["serie"] //Esto ya es un String
	res := models.ListarOrdenTrabajo(idorden)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
