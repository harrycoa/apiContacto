package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"../models"
	"github.com/gorilla/mux"
)

// Listar Usos vehiculares
func ListarRazonNoCitas(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	res := models.ListarRazonNoCitas()
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarRazonNoCita(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	idMotivo := vars["idMotivo"]
	res := models.ListarRazonNoCita(idMotivo)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
