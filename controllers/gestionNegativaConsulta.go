package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"../models"
	"github.com/gorilla/mux"
)

// Listar Usos vehiculares
func ListarGestionConsultas(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	res := models.ListarGestionNegativaConsultas()
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}

func ListarGestionConsulta(w http.ResponseWriter, r *http.Request) {
	models.CreateConnection()
	vars := mux.Vars(r)
	placa := vars["placa"]
	res := models.ListarGestionNegativaConsulta(placa)
	j, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(j)
	w.WriteHeader(http.StatusOK)
	models.Closeconnection()
}
