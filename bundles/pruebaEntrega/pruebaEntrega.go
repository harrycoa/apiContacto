package pruebaEntrega

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"

	db "../../configuration"
	"../../results"
)

// Fecha ...
type PruebaEntrega struct {
	IdVehiculoEntrega     int64  `json:"idVehiculoEntrega"`
	Modelo                string `json:"serie"`
	Motor                 string `json:"motor"`
	Chasis                string `json:"chasis"`
	Vin                   string `json:"vin"`
	AnioFabricacion       string `json:"anioFabricacion"`
	PersonaEntrega        string `json:"personaEntrega"`
	Telefono              string `json:"telefono"`
	Celular               string `json:"celular"`
	OpinionEntrega        string `json:"opinionEntrega"`
	FechaEntregaVehiculo  string `json:"fechaEntregaVehiculo"`
	FechaCitaReferencial2 string `json:"fechaCitaReferencial2"`
}

type PruebaEntregas []PruebaEntrega

// Router ...
func Router(router *mux.Router) {
	prefix := "/api/pruebaEntrega"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", Listar).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}

func Listar(w http.ResponseWriter, r *http.Request) {

	rows, err := db.SpExecQuery("SELECT * FROM v_vehiculoentrega")
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	list := setList(rows)
	rows.Close()

	j, err := json.Marshal(list)
	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func setList(rows *sql.Rows) PruebaEntregas {
	listado := PruebaEntregas{}
	for rows.Next() {
		obj := PruebaEntrega{}
		rows.Scan(&obj.IdVehiculoEntrega,
			&obj.Modelo,
			&obj.Motor,
			&obj.Chasis,
			&obj.Vin,
			&obj.AnioFabricacion,
			&obj.PersonaEntrega,
			&obj.Telefono,
			&obj.Celular,
			&obj.OpinionEntrega,
			&obj.FechaEntregaVehiculo,
			&obj.FechaCitaReferencial2)
		listado = append(listado, obj)
	}
	return listado

}
