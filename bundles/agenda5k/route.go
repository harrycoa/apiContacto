package agenda5k

import (
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// ListarAgenda5kRouter ...
func Router(router *mux.Router) {
	prefix := "/api/agenda5k"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", ListarAgenda5k).Methods("GET")
	// subrouter.HandleFunc("/{fecha1}/{fecha2}", ListarAgenda5kParametro).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
