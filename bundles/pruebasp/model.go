package pruebasp

type OrdenReparacion struct {
	Placa           string `json:"placa"`
	Numero          int64  `json:"numero"`
	Nombre          string `json:"nombre"`
	Km              int64  `json:"km"`
	Servicio        string `json:"servicio"`
	Telefono        string `json:"telefono"`
	FechaInicio     string `json:"fechaInicio"`
	FechaFin        string `json:"fechaFin"`
	Serie           string `json:"serie"`
	Motor           string `json:"motor"`
	Chasis          string `json:"chasis"`
	Vin             string `json:"vin"`
	AnioFabricacion string `json:"anioFabricacion"`
}

// OrdenReparaciones es un tipo
type OrdenReparaciones []OrdenReparacion
