package pruebasp

import (
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// PruebaAccesoBdRouter .......
func PruebaAccesoBdRouter(router *mux.Router) {
	prefix := "/accesoBd"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", PruebaAccesoBdNative).Methods("GET")
	subrouter.HandleFunc("/{placa}", PruebaAccesoBdOtherLib).Methods("GET")

	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
