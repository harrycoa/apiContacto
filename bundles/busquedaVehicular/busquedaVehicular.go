package busquedaVehicular

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"

	db "../../configuration"
	"../../results"
)

type BusquedaVehicular struct {
	Serie           string `json:"serie"`
	Placa           string `json:"placa"`
	Motor           string `json:"motor"`
	Chasis          string `json:"chasis"`
	Vin             string `json:"vin"`
	AnioFabricacion string `json:"anioFabricacion"`
	Modelo          string `json:"modelo"`
	Cliente         string `json:"cliente"`
	Telefono        string `json:"telefono"`
}

// Router
func Router(router *mux.Router) {
	prefix := "/api/busquedaVehicular/{serie}"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", BuscarVehiculo).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}

// Controller
func BuscarVehiculo(w http.ResponseWriter, r *http.Request) {
	param := mux.Vars(r)
	serie := param["serie"]

	row, err := db.SpExecQueryRow("CALL spBusquedaVehicular(?)", serie)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	res := BusquedaVehicular{}
	err = row.Scan(&res.Serie,
		&res.Placa,
		&res.Motor,
		&res.Chasis,
		&res.Vin,
		&res.AnioFabricacion,
		&res.Modelo,
		&res.Cliente,
		&res.Telefono)

	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	j, err := json.Marshal(res)
	if err != nil {
		results.ErrorResult(w, err.Error())
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}
