package formulario

import (
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// Insertar ...
func Router(router *mux.Router) {
	prefix := "/api/insertarFormulario"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", InsertarForm).Methods("POST")
	// subrouter.HandleFunc("/{fecha1}/{fecha2}", ListarAgenda5kParametro).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
