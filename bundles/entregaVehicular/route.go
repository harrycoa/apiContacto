package entregaVehicular

import (
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// Insertar ...
func EntregaRouter(router *mux.Router) {
	prefix := "/api/entregaVehicular"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", InsertarEntrega).Methods("POST")
	subrouter.HandleFunc("/{serie}", VerificarEntrega).Methods("GET")
	// subrouter.HandleFunc("/{fecha1}/{fecha2}", ListarAgenda5kParametro).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}

// Buscar
func BusquedaEntregaRouter(router *mux.Router) {
	prefix := "/api/bentrega"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("/{serie}", VehiculoEntregado).Methods("GET")
	// subrouter.HandleFunc("/{fecha1}/{fecha2}", ListarAgenda5kParametro).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
