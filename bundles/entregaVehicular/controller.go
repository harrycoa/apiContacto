package entregaVehicular

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	db "../../configuration"
	"../../results"
	"github.com/gorilla/mux"
)

// Insertar
func InsertarEntrega(w http.ResponseWriter, r *http.Request) {

	entregaVehicular := EntregaVehicular{}
	err := json.NewDecoder(r.Body).Decode(&entregaVehicular)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	row, err := guardar(entregaVehicular)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	res := results.Resultado{}
	fmt.Println(row)
	err = row.Scan(&res.Id, &res.Mensaje)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	if res.Id == 0 {
		results.CreatedResult(w, res.Mensaje)
	} else {

		j, _ := json.Marshal(res)
		w.WriteHeader(http.StatusPreconditionFailed)
		w.Write(j)
	}

}

// VerificarEntrega ...
func VerificarEntrega(w http.ResponseWriter, r *http.Request) {
	param := mux.Vars(r)
	serie := param["serie"]

	row, err := db.SpExecQueryRow("CALL spVerificarExisteEntrega(?)", serie)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	res := results.Resultado{}

	err = row.Scan(&res.Id, &res.Mensaje)

	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	j, err := json.Marshal(res)
	if err != nil {
		results.ErrorResult(w, err.Error())
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)

}

// ListarEntrega ...
func VehiculoEntregado(w http.ResponseWriter, r *http.Request) {
	param := mux.Vars(r)
	serie := param["serie"]

	// crear objeto resultado
	type ResultadoEntregaVehicular struct {
		EntregaVehicular EntregaVehicular  `json:"entregaVehicular"`
		Resultado        results.Resultado `json:"resultado"`
	}

	row, err := db.SpExecQueryRow("CALL spBusquedaEntrega(?)", serie)
	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	res := ResultadoEntregaVehicular{}

	err = row.Scan(&res.Resultado.Id,
		&res.Resultado.Mensaje,
		&res.EntregaVehicular.PersonaEntrega,
		&res.EntregaVehicular.Telefono,
		&res.EntregaVehicular.OpinionEntrega,
		&res.EntregaVehicular.FechaEntregaVehiculo)

	if err != nil {
		results.ErrorResult(w, err.Error())
		return
	}

	j, err := json.Marshal(res)
	if err != nil {
		results.ErrorResult(w, err.Error())
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

// Guardar
func guardar(e EntregaVehicular) (*sql.Row, error) {
	fechaE, _ := time.Parse(time.RFC3339, e.FechaEntregaVehiculo)

	// "CALL spInsertarEntrega(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
	spName := db.SpName("spInsertarEntrega", 17)
	row, err := db.SpExecQueryRow(spName,
		e.Serie,
		e.Placa,
		e.Motor,
		e.Chasis,
		e.Vin,
		e.AnioFabricacion,
		e.Modelo,
		e.FechaProgramacionEntrega,
		fechaE,
		e.PersonaEntrega,
		e.Telefono,
		e.Celular,
		e.OpinionEntrega,
		e.FechaCitaReferencial2,
		e.UsoVehicular,
		e.IdDepartamento,
		e.IdProvincia)

	return row, err
}
