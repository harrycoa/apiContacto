package commons

import (
	"crypto/rsa"
	"io/ioutil"
	"log"
	"time"

	"../models/auth"
	"../models/user"

	jwt "github.com/dgrijalva/jwt-go"
)

var (
	privateKey *rsa.PrivateKey
	// PublicKey se usa para validar el token
	PublicKey *rsa.PublicKey
)

func init() {

	privateBytes, err := ioutil.ReadFile("./keys/private.rsa")
	if err != nil {
		log.Fatal("No se pudo leer la llave privada")
	}

	publicBytes, err := ioutil.ReadFile("./keys/public.rsa")
	if err != nil {
		log.Fatal("No se pudo leer la llave pública")
	}

	privateKey, err = jwt.ParseRSAPrivateKeyFromPEM(privateBytes)
	if err != nil {
		log.Fatal("Error al parsear privateKey", err)
	}

	PublicKey, err = jwt.ParseRSAPublicKeyFromPEM(publicBytes)
	if err != nil {
		log.Fatal("Error al parsear PublicKey", err)
	}
}

// GenerateJWT genera un token
func GenerateJWT(user user.TokenUser) string {

	claims := auth.Claim{
		StandardClaims: jwt.StandardClaims{
			Issuer:    "DevBugSoft",
			Audience:  "appContacto",
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(time.Hour * 2).Unix(),
		},
		TokenUser: user,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	result, err := token.SignedString(privateKey)

	if err != nil {
		log.Fatal("Error al firmar el token")
	}
	return result
}
