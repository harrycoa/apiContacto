package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func PresupuestoRouter(router *mux.Router) {
	prefix := "/api/presupuesto"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.InsertarPresupuesto).Methods("POST")
	subrouter.HandleFunc("", controllers.ListarPresupuestos).Methods("GET")
	subrouter.HandleFunc("/{numeroPresupuesto}", controllers.ListarPresupuesto).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
