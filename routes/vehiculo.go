package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func VehiculoRouter(router *mux.Router) {
	prefix := "/api/vehiculo"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.InsertarVehiculo).Methods("POST")
	subrouter.HandleFunc("", controllers.ListarVehiculos).Methods("GET")
	subrouter.HandleFunc("/{claseServicio}", controllers.ListarVehiculo).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
