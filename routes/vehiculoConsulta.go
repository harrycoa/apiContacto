package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func ConsultaVehicularRouter(router *mux.Router) {
	prefix := "/api/consultavehiculo"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.ListarVehiculosConsulta).Methods("GET")
	subrouter.HandleFunc("/{tdp}", controllers.ListarVehiculoConsulta).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
