package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func MotivoResultadoRouter(router *mux.Router) {
	prefix := "/api/resultado"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.ListarResultados).Methods("GET")
	subrouter.HandleFunc("/{idMotivo}", controllers.ListarResultado).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
