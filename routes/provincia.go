package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func ProvinciaRouter(router *mux.Router) {
	prefix := "/api/provincia"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.ListarProvincias).Methods("GET")
	subrouter.HandleFunc("/{idDepartamento}", controllers.ListarProvincia).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
