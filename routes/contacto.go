package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func ContactoRouter(router *mux.Router) {
	prefix := "/api/contacto"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.InsertarContacto).Methods("POST")
	subrouter.HandleFunc("", controllers.ActualizarContacto).Methods("PUT")
	subrouter.HandleFunc("", controllers.EliminarContacto).Methods("DELETE")
	subrouter.HandleFunc("", controllers.ListarContacto).Methods("GET")
	subrouter.HandleFunc("/{placa}", controllers.ListarContactoParametro).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
