package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// PruebasRouter pruebas
func PruebasRouter(router *mux.Router) {
	prefix := "/pruebas"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.Pruebas).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
