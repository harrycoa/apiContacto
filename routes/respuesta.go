package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func RespuestasRouter(router *mux.Router) {
	prefix := "/api/respuesta"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.ListarRespuestas).Methods("GET")
	subrouter.HandleFunc("", controllers.InsertarRespuesta).Methods("POST")
	subrouter.HandleFunc("/{respuesta}", controllers.ListarRespuestaParametro).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
