package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func InsertarFormularioRouter(router *mux.Router) {
	prefix := "/api/form"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.InsertarFormulario).Methods("POST")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
