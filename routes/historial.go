package routes

import (
	"../controllers"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func HistorialContRouter(router *mux.Router) {
	prefix := "/api/historialC"
	subrouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	subrouter.HandleFunc("", controllers.HistorialContactoCont).Methods("GET")
	subrouter.HandleFunc("/{fecha1}/{fecha2}", controllers.HistorialContactoParametroCont).Methods("GET")
	subrouter.HandleFunc("/{placa}", controllers.HistorialContactoParametroPlaca).Methods("GET")
	router.PathPrefix(prefix).Handler(
		negroni.New(
			// negroni.HandlerFunc(controllers.ValidateToken),
			negroni.Wrap(subrouter),
		),
	)
}
