package configuration

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	// driver mysql controller
	_ "github.com/go-sql-driver/mysql"
)

// Configuration clase de configuración
type Configuration struct {
	Server   string
	Port     string
	User     string
	Password string
	Database string
}

var databaseconfig *Configuration

// getConfiguration obtiene la configuración de BD.
func getConfiguration() Configuration {

	var c Configuration

	file, err := os.Open("./config.json")
	checkErr(err)

	defer file.Close()

	err = json.NewDecoder(file).Decode(&c)
	checkErr(err)
	return c

}

// OpenDB devuelve la conexión a la BD
func OpenDB() (*sql.DB, error) {
	c := getConfiguration()
	//data source name
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true&loc=Local",
		c.User, c.Password, c.Server, c.Port, c.Database)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func (this *Configuration) url() string {
	c := getConfiguration()
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true&loc=Local",
		c.User, c.Password, c.Server, c.Port, c.Database)
}

func GetUrlDatabase() string {
	return databaseconfig.url()
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// SpExec ejecuta un sp, de tipo insert y update,delete o que no devuelva filas.
func SpExec(procedimiento string, params ...interface{}) (sql.Result, error) {

	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	result, err := db.Exec(procedimiento, params...)
	return result, err
}

// SpExecQuery ejecuta un sp que devuelve un conjunto de rows, cerrar usando row.Close()
func SpExecQuery(procedimiento string, params ...interface{}) (*sql.Rows, error) {

	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := db.Query(procedimiento, params...)

	return rows, err
}

// SpExecQueryRow ejecuta un sp que devuelve un row
func SpExecQueryRow(procedimiento string, params ...interface{}) (*sql.Row, error) {

	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	row := db.QueryRow(procedimiento, params...)

	return row, nil
}

func SpName(spName string, nroParams int) string {

	spPrefix := "CALL " + spName + "(?"

	if nroParams == 1 {
		return spPrefix + ")"
	}

	var b bytes.Buffer
	for i := 1; i < nroParams; i++ {
		b.WriteString(",?")
	}
	return spPrefix + b.String() + ")"
}

func IsStr(s string) interface{} {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return s
}

func IsDate(v interface{}) interface{} {
	if len(v.(string)) == 0 {
		return sql.NullString{}
	}
	fecha, _ := time.Parse(time.RFC3339, v.(string))
	return fecha
}
