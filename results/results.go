package results

import (
	"encoding/json"
	"net/http"

	"../models"
)

// CreatedResult ...
func CreatedResult(w http.ResponseWriter, message string) {

	m := models.Message{Code: http.StatusCreated, Message: message}
	j, _ := json.Marshal(m)

	w.WriteHeader(http.StatusCreated)
	w.Write(j)
}

// ErrorResult devuelve mensaje de error al cliente 500
func ErrorResult(w http.ResponseWriter, message string) {

	m := models.Message{Code: http.StatusInternalServerError,
		Message: message}
	j, _ := json.Marshal(m)

	w.WriteHeader(http.StatusInternalServerError)
	w.Write(j)
}

// EmptyResult devuelve mensaje de recurso creado 204
func EmptyResult(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNoContent)
}

// DecodeBody ...
func DecodeBody(w http.ResponseWriter, err error) {
	if err != nil {
		m := models.Message{Code: http.StatusBadRequest, Message: err.Error()}
		j, _ := json.Marshal(m)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
	}
}

// func checkJSONParse(err error) {
// 	if err != nil {
// 		log.Fatalf("Error al convertir el mensaje: %s", err)
// 	}
// }
