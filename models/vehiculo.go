package models

type Vehiculo struct {
	IdVehiculo      int64  `json:"idVehiculo"`
	Serie           string `json:"serie"`
	Placa           string `json:"placa"`
	Motor           string `json:"motor"`
	Chasis          string `json:"chasis"`
	Vin             string `json:"vin"`
	AnioFabricacion string `json:"anioFabricacion"`
	Modelo          string `json:"modelo"`
}

const vehiculoEsquema string = `CREATE TABLE x_tavehiculo (
	idVehiculo int(11) NOT NULL AUTO_INCREMENT,
	serie varchar(6) DEFAULT NULL,
	placa varchar(6) DEFAULT NULL,
	motor varchar(100) DEFAULT NULL,
	chasis varchar(200) DEFAULT NULL,
	vin varchar(20) DEFAULT NULL,
	anioFabricacion varchar(4) DEFAULT NULL,
	modelo varchar(100) NULL,
	PRIMARY KEY (idVehiculo)
  );
  `

type Vehiculos []Vehiculo

func ListarVehiculos() Vehiculos {
	sql := `SELECT idVehiculo, 
					serie, 
					placa, 
					motor, 
					chasis, 
					vin, 
					anioFabricacion, 
					modelo FROM x_tavehiculo`
	listado := Vehiculos{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Vehiculo{}
		rows.Scan(&obj.IdVehiculo,
			&obj.Serie,
			&obj.Placa,
			&obj.Motor,
			&obj.Chasis,
			&obj.Vin,
			&obj.AnioFabricacion,
			&obj.Modelo)
		listado = append(listado, obj)
	}
	return listado
}

func ListarVehiculo(serie string) Vehiculos {
	sql := `SELECT idVehiculo, 
					serie, 
					placa, 
					motor, 
					chasis, 
					vin, 
					anioFabricacion, 
					modelo FROM x_tavehiculo WHERE serie=?`
	listado := Vehiculos{}
	rows, _ := Query(sql, serie)
	for rows.Next() {
		obj := Vehiculo{}
		rows.Scan(&obj.IdVehiculo,
			&obj.Serie,
			&obj.Placa,
			&obj.Motor,
			&obj.Chasis,
			&obj.Vin,
			&obj.AnioFabricacion,
			&obj.Modelo)
		listado = append(listado, obj)
	}
	return listado
}

func NuevoVehiculo(
	serie,
	placa,
	motor,
	chasis,
	vin,
	anioFabricacion,
	modelo string) *Vehiculo {
	objvehiculo := &Vehiculo{
		IdVehiculo:      0,
		Serie:           serie,
		Placa:           placa,
		Motor:           motor,
		Chasis:          chasis,
		Vin:             vin,
		AnioFabricacion: anioFabricacion,
		Modelo:          modelo,
	}
	return objvehiculo
}

func CrearVehiculo(
	serie,
	placa,
	motor,
	chasis,
	vin,
	anioFabricacion,
	modelo string) *Vehiculo {
	objvehiculo := NuevoVehiculo(
		serie,
		placa,
		motor,
		chasis,
		vin,
		anioFabricacion,
		modelo)
	objvehiculo.Guardar()
	return objvehiculo
}

func (this *Vehiculo) Guardar() {
	if this.IdVehiculo == 0 {
		this.IdVehiculo = this.insertar()
	} else {
		this.actualizar()
	}
}

func (this *Vehiculo) insertar() int64 {

	sql := `INSERT x_tavehiculo SET serie=?, 
									placa=?,
									motor=?, 
									chasis=?, 
									vin=?,
									anioFabricacion=?,
									modelo=?`
	result, err := Exec(sql,
		this.Serie,
		this.Placa,
		this.Motor,
		this.Chasis,
		this.Vin,
		this.AnioFabricacion,
		this.Modelo)
	this.IdVehiculo, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
		return 0
	} else {
		println("NUEVO ID VEHICULO:", this.IdVehiculo)
		return this.IdVehiculo
	}
}

func (this *Vehiculo) actualizar() {
	sql := `UPDATE x_tavehiculo SET serie=?, 
									placa=?
									motor=?, 
									chasis=?, 
									vin=?,
									anioFabricacion=?,
									modelo=?`
	Exec(sql,
		this.Serie,
		this.Placa,
		this.Motor,
		this.Chasis,
		this.Vin,
		this.AnioFabricacion,
		this.Modelo)
}
