package models

type MotivoResultado struct {
	IdMotivoResultado int64  `json:"idMotivoResultado"`
	ResultadoMotivo   string `json:"resultadoMotivo"`
	IdMotivo          int64
}

const motivoResultadoEsquema string = `CREATE TABLE x_tamotivoresultado (
	idMotivoResultado int(11) NOT NULL AUTO_INCREMENT,
	resultadoMotivo varchar(200) NOT NULL,
	idMotivo int(11) DEFAULT NULL,
	PRIMARY KEY (idMotivoResultado)
  );`

type MotivoResultados []MotivoResultado

// constructores
func ListarResultados() MotivoResultados {
	sql := "SELECT idMotivoResultado, resultadoMotivo, idMotivo FROM x_tamotivoresultado"
	listado := MotivoResultados{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := MotivoResultado{}
		rows.Scan(&obj.IdMotivoResultado, &obj.ResultadoMotivo, &obj.IdMotivo)
		listado = append(listado, obj)
	}
	return listado
}

func ListarResultado(idMotivo2 string) MotivoResultados {
	sql := "SELECT idMotivoResultado, resultadoMotivo, idMotivo FROM x_tamotivoresultado WHERE idMotivo=?"
	listado := MotivoResultados{}
	rows, _ := Query(sql, idMotivo2)
	for rows.Next() {
		obj := MotivoResultado{}
		rows.Scan(&obj.IdMotivoResultado, &obj.ResultadoMotivo, &obj.IdMotivo)
		listado = append(listado, obj)
	}
	return listado
}
