package models

import "time"

type NoCita struct {
	IdNoCita              int64   `json:"idRazon"`
	ClaseRazon            string  `json:"claseRazon"`
	DescripcionNoCita     string  `json:"descripcionNoCita"`
	SabeKm                string  `json:"sabeKm"`
	KmFaltaRecorrido      int64   `json:"kmFaltaRecorrido"`
	SabePromedio          string  `json:"sabePromedio"`
	PromedioRecorrido     float64 `json:"promedioRecorrido"`
	UsoVehicular          string  `json:"usoVehicular"`
	FechaPropuesta        string  `json:"fechaPropuesta"`
	FechaSugeridaCliente  string  `json:"fechaSugeridaCliente"`
	FechaCorregida        string  `json:"fechaCorregida"`
	FechaFinal            string  `json:"fechaFinal"`
	FechaSiguienteLlamada string  `json:"fechaSiguienteLlamada"`
	Concesionario         string  `json:"concesionario"`
	CausaRechazoCita      string  `json:"causaRechazoCita"`
	CausaRechazoCorasur   string  `json:"causaRechazoCorasur"`
	ObservacionesNoCita   string  `json:"observacionesNoCita"`
	Comentarios           string  `json:"comentarios"`
	IdRazonNoCita         int64
}

type NoCitas []NoCita

const noCitaEsquema string = `CREATE TABLE x_tanocita (
	-- tabla razon no Cita
	idNoCita int(11) NOT NULL AUTO_INCREMENT,
	claseRazon varchar(100) NOT NULL, -- clase A,b - asociados
	descripcionNoCita varchar(100) NOT NULL,  -- no recibio tarjeta, falta recorrido	
	sabeKm char(1) NOT NULL, 
	kmFaltaRecorrido int(100) NULL,
	sabePromedio char(1) NOT NULL, 
	promedioRecorrido double NULL,
	usoVehicular varchar(100) NOT NULL,
	fechaPropuesta datetime NULL,
	fechaSugeridaCliente datetime NULL, -- cuando le gustaria que le llamen ?
	fechaCorregida datetime NULL,
	fechaFinal datetime NULL,
	fechaSiguienteLlamada datetime NULL,
	concesionario varchar(200) NULL, 
	causaRechazoCita varchar(200) NULL, -- razon de nodesea cita
	causaRechazoCorasur varchar(200) NULL, -- razon de nodesea corasur
	observacionesNoCita varchar(200) NULL, 
	comentarios  varchar(200) NULL,   
	idRazonNoCita int(11) NULL,
	PRIMARY KEY (idRazon)
  );`

// Constructores
func NuevaNoCita(claseRazon,
	descripcionNoCita,
	sabeKm string,
	kmFaltaRecorrido int64,
	sabePromedio string,
	promedioRecorrido float64,
	usoVehicular,
	fechaPropuesta,
	fechaSugeridaCliente,
	fechaCorregida,
	fechaFinal,
	fechaSiguienteLlamada,
	concesionario,
	causaRechazoCita,
	causaRechazoCorasur,
	observacionesNoCita,
	comentarios string,
	idRazonNoCita int64) *NoCita {
	obj := &NoCita{ClaseRazon: claseRazon,
		DescripcionNoCita:     descripcionNoCita,
		SabeKm:                sabeKm,
		KmFaltaRecorrido:      kmFaltaRecorrido,
		SabePromedio:          sabePromedio,
		PromedioRecorrido:     promedioRecorrido,
		UsoVehicular:          usoVehicular,
		FechaPropuesta:        fechaPropuesta,
		FechaSugeridaCliente:  fechaSugeridaCliente,
		FechaCorregida:        fechaCorregida,
		FechaFinal:            fechaFinal,
		FechaSiguienteLlamada: fechaSiguienteLlamada,
		Concesionario:         concesionario,
		CausaRechazoCita:      causaRechazoCita,
		CausaRechazoCorasur:   causaRechazoCorasur,
		ObservacionesNoCita:   observacionesNoCita,
		Comentarios:           comentarios,
		IdRazonNoCita:         idRazonNoCita}
	return obj
}

func CrearNoCita(claseRazon,
	descripcionNoCita,
	sabeKm string,
	kmFaltaRecorrido int64,
	sabePromedio string,
	promedioRecorrido float64,
	usoVehicular,
	fechaPropuesta,
	fechaSugeridaCliente,
	fechaCorregida,
	fechaFinal,
	fechaSiguienteLlamada,
	concesionario,
	causaRechazoCita,
	causaRechazoCorasur,
	observacionesNoCita,
	comentarios string,
	idRazonNoCita int64) *NoCita {
	obj := NuevaNoCita(claseRazon,
		descripcionNoCita,
		sabeKm,
		kmFaltaRecorrido,
		sabePromedio,
		promedioRecorrido,
		usoVehicular,
		fechaPropuesta,
		fechaSugeridaCliente,
		fechaCorregida,
		fechaFinal,
		fechaSiguienteLlamada,
		concesionario,
		causaRechazoCita,
		causaRechazoCorasur,
		observacionesNoCita,
		comentarios,
		idRazonNoCita)
	obj.Guardar()
	return obj
}

func ListarNoCita(id int) *NoCita {
	obj := NuevaNoCita("", "", "", 0, "", 0, "", "", "", "", "", "", "", "", "", "", "", 0)
	sql := `SELECT idNoCita, 
				   claseRazon, 
				   descripcionNoCita, 				   
				   sabeKm, 
				   kmFaltaRecorrido, 
				   sabePromedio, 
				   promedioRecorrido, 
				   usoVehicular, 
				   fechaPropuesta, 
				   fechaSugeridaCliente, 
				   fechaCorregida, 
				   fechaFinal, 
				   fechaSiguienteLlamada, 
				   concesionario, 
				   causaRechazoCita, 
				   causaRechazoCorasur, 
				   observacionesNoCita,
				   comentarios,
				   idRazonNoCita FROM x_tanocita where idNoCita=?`
	rows, _ := Query(sql, id)
	for rows.Next() {
		rows.Scan(&obj.IdNoCita,
			&obj.ClaseRazon,
			&obj.DescripcionNoCita,
			&obj.SabeKm,
			&obj.KmFaltaRecorrido,
			&obj.SabePromedio,
			&obj.PromedioRecorrido,
			&obj.UsoVehicular,
			&obj.FechaPropuesta,
			&obj.FechaSugeridaCliente,
			&obj.FechaCorregida,
			&obj.FechaFinal,
			&obj.FechaSiguienteLlamada,
			&obj.Concesionario,
			&obj.CausaRechazoCita,
			&obj.CausaRechazoCorasur,
			&obj.ObservacionesNoCita,
			&obj.Comentarios,
			&obj.IdRazonNoCita)
	}
	return obj
}
func ListarNoCitas() NoCitas {
	sql := `SELECT idNoCita, 
				   claseRazon, 
				   descripcionNoCita, 		
				   sabeKm, 
				   kmFaltaRecorrido, 
				   sabePromedio, 
				   promedioRecorrido, 
				   usoVehicular, 
				   fechaPropuesta, 
				   fechaSugeridaCliente, 
				   fechaCorregida, 
				   fechaFinal, 
				   fechaSiguienteLlamada, 
				   concesionario, 
				   causaRechazoCita, 
				   causaRechazoCorasur, 
				   observacionesNoCita, 
				   comentarios,
				   idRazonNoCita FROM x_tanocita`
	listado := NoCitas{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := NoCita{}
		rows.Scan(&obj.IdNoCita,
			&obj.ClaseRazon,
			&obj.DescripcionNoCita,
			&obj.SabeKm,
			&obj.KmFaltaRecorrido,
			&obj.SabePromedio,
			&obj.PromedioRecorrido,
			&obj.UsoVehicular,
			&obj.FechaPropuesta,
			&obj.FechaSugeridaCliente,
			&obj.FechaCorregida,
			&obj.FechaFinal,
			&obj.FechaSiguienteLlamada,
			&obj.Concesionario,
			&obj.CausaRechazoCita,
			&obj.CausaRechazoCorasur,
			&obj.ObservacionesNoCita,
			&obj.Comentarios,
			&obj.IdRazonNoCita)
		listado = append(listado, obj)
	}
	return listado
}

func ListarNoCitasParametro(claseRazon string) NoCitas {
	sql := `SELECT idNoCita, 
				   claseRazon, 
				   descripcionNoCita, 				
				   sabeKm, 
				   kmFaltaRecorrido, 
				   sabePromedio, 
				   promedioRecorrido, 
				   usoVehicular, 
				   fechaPropuesta, 
				   fechaSugeridaCliente, 
				   fechaCorregida, 
				   fechaFinal, 
				   fechaSiguienteLlamada, 
				   concesionario, 
				   causaRechazoCita, 
				   causaRechazoCorasur, 
				   observacionesNoCita, 
				   comentarios,
				   idRazonNoCita 
				   FROM x_tanocita where claseRazon=?`
	listado := NoCitas{}
	rows, _ := Query(sql, claseRazon)
	for rows.Next() {
		obj := NoCita{}
		rows.Scan(&obj.IdNoCita,
			&obj.ClaseRazon,
			&obj.DescripcionNoCita,
			&obj.SabeKm,
			&obj.KmFaltaRecorrido,
			&obj.SabePromedio,
			&obj.PromedioRecorrido,
			&obj.UsoVehicular,
			&obj.FechaPropuesta,
			&obj.FechaSugeridaCliente,
			&obj.FechaCorregida,
			&obj.FechaFinal,
			&obj.FechaSiguienteLlamada,
			&obj.Concesionario,
			&obj.CausaRechazoCita,
			&obj.CausaRechazoCorasur,
			&obj.ObservacionesNoCita,
			&obj.Comentarios,
			&obj.IdRazonNoCita)
		listado = append(listado, obj)
	}
	return listado
}

func (this *NoCita) Guardar() {

	if this.IdNoCita == 0 {
		this.insertar()
	} else {
		this.actualizar()
	}
}

func (this *NoCita) insertar() {
	sql := `INSERT x_tanocita SET claseRazon=?, 
								 descripcionNoCita=?, 							
								 sabeKm=?, 
								 kmFaltaRecorrido=?, 
								 sabePromedio=?, 
								 promedioRecorrido=?, 
								 usoVehicular=?, 
								 fechaPropuesta=?, 
								 fechaSugeridaCliente=?, 
								 fechaCorregida=?, 
								 fechaFinal=?, 
								 fechaSiguienteLlamada=?, 
								 concesionario=?, 
								 causaRechazoCita=?, 
								 causaRechazoCorasur=?, 
								 observacionesNoCita=?, 
								 comentarios=?,
								 idRazonNoCita=?`
	fechaPropuesta, _ := time.Parse(time.RFC3339, this.FechaPropuesta)
	fechaSugeridaCliente, _ := time.Parse(time.RFC3339, this.FechaSugeridaCliente)
	fechaCorregida, _ := time.Parse(time.RFC3339, this.FechaCorregida)
	fechaFinal, _ := time.Parse(time.RFC3339, this.FechaFinal)
	fechaSiguienteLlamada, _ := time.Parse(time.RFC3339, this.FechaSiguienteLlamada)
	result, _ := Exec(sql, this.ClaseRazon,
		this.DescripcionNoCita,
		this.SabeKm,
		this.KmFaltaRecorrido,
		this.SabePromedio,
		this.PromedioRecorrido,
		this.UsoVehicular,
		fechaPropuesta,
		fechaSugeridaCliente,
		fechaCorregida,
		fechaFinal,
		fechaSiguienteLlamada,
		this.Concesionario,
		this.CausaRechazoCita,
		this.CausaRechazoCorasur,
		this.ObservacionesNoCita,
		this.Comentarios,
		this.IdRazonNoCita)
	this.IdNoCita, _ = result.LastInsertId()
}

func (this *NoCita) actualizar() {
	sql := `UPDATE x_tanocita SET claseRazon=?, 
								 descripcionNoCita=?, 								
								 sabeKm=?, 
								 kmFaltaRecorrido=?, 
								 sabePromedio=?, 
								 promedioRecorrido=?, 
								 usoVehicular=?, 
								 fechaPropuesta=?, 
								 fechaSugeridaCliente=?, 
								 fechaCorregida=?, 
								 fechaFinal=?, 
								 fechaSiguienteLlamada=?, 
								 concesionario=?, 
								 causaRechazoCita=?, 
								 causaRechazoCorasur=?, 
								 observacionesNoCita=?, 
								 comentarios=?,
								 idRazonNoCita=?`
	fechaPropuesta, _ := time.Parse(time.RFC3339, this.FechaPropuesta)
	fechaSugeridaCliente, _ := time.Parse(time.RFC3339, this.FechaSugeridaCliente)
	fechaCorregida, _ := time.Parse(time.RFC3339, this.FechaCorregida)
	fechaFinal, _ := time.Parse(time.RFC3339, this.FechaFinal)
	fechaSiguienteLlamada, _ := time.Parse(time.RFC3339, this.FechaSiguienteLlamada)
	Exec(sql, this.ClaseRazon, this.DescripcionNoCita,
		this.SabeKm,
		this.KmFaltaRecorrido,
		this.SabePromedio,
		this.PromedioRecorrido,
		this.UsoVehicular,
		fechaPropuesta,
		fechaSugeridaCliente,
		fechaCorregida,
		fechaFinal,
		fechaSiguienteLlamada,
		this.Concesionario,
		this.CausaRechazoCita,
		this.CausaRechazoCorasur,
		this.ObservacionesNoCita,
		this.Comentarios,
		this.IdRazonNoCita)
}

func (this *NoCita) Delete() {
	sql := "DELETE FROM x_tanocita WHERE idNoCita=?"
	Exec(sql, this.IdNoCita)
}
