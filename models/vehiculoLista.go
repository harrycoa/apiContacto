package models

type VehiculoLista struct {
	IdVehiculoEntrega     int64  `json:"idVehiculoEntrega"`
	Modelo                string `json:"serie"`
	Motor                 string `json:"motor"`
	Chasis                string `json:"chasis"`
	Vin                   string `json:"vin"`
	AnioFabricacion       string `json:"anioFabricacion"`
	PersonaEntrega        string `json:"personaEntrega"`
	Telefono              string `json:"telefono"`
	Celular               string `json:"celular"`
	OpinionEntrega        string `json:"opinionEntrega"`
	FechaEntregaVehiculo  string `json:"fechaEntregaVehiculo"`
	FechaCitaReferencial2 string `json:"fechaCitaReferencial2"`
}

type VehiculoListas []VehiculoLista

func VehiculosLista() VehiculoListas {
	sql := `select x_tavehiculoentrega.idVehiculoEntrega,
	x_tavehiculo.serie,
	x_tavehiculo.motor,
	x_tavehiculo.chasis,
	x_tavehiculo.vin,
	x_tavehiculo.anioFabricacion,
	x_tavehiculoentrega.personaEntrega,
	x_tavehiculoentrega.telefono,
	x_tavehiculoentrega.celular,
	x_tavehiculoentrega.opinionEntrega,
	x_tavehiculoentrega.fechaEntregaVehiculo, 
	x_tavehiculoentrega.fechaCitaReferencial2
from x_tavehiculo  inner join x_tavehiculoentrega
on  x_tavehiculo.idVehiculo=x_tavehiculoentrega.idVehiculo
order by fechaEntregaVehiculo desc`
	listado := VehiculoListas{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := VehiculoLista{}
		rows.Scan(&obj.IdVehiculoEntrega,
			&obj.Modelo,
			&obj.Motor,
			&obj.Chasis,
			&obj.Vin,
			&obj.AnioFabricacion,
			&obj.PersonaEntrega,
			&obj.Telefono,
			&obj.Celular,
			&obj.OpinionEntrega,
			&obj.FechaEntregaVehiculo,
			&obj.FechaCitaReferencial2)
		listado = append(listado, obj)
	}
	return listado
}

func VehiculosListaParametro(serie string) VehiculoListas {
	sql := `select x_tavehiculoentrega.personaEntrega,
	x_tavehiculoentrega.telefono,
	x_tavehiculoentrega.opinionEntrega,
	x_tavehiculoentrega.fechaEntregaVehiculo
from x_tavehiculo  inner join x_tavehiculoentrega
on  x_tavehiculo.idVehiculo=x_tavehiculoentrega.idVehiculo where x_tavehiculo.serie=?`
	listado := VehiculoListas{}
	rows, _ := Query(sql, serie)
	for rows.Next() {
		obj := VehiculoLista{}
		rows.Scan(&obj.PersonaEntrega,
			&obj.Telefono,
			&obj.OpinionEntrega,
			&obj.FechaEntregaVehiculo)
		listado = append(listado, obj)
	}
	return listado
}
