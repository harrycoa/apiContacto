package models

import "time"

type OrdenTrabajo struct {
	IdOrdenTrabajo     int64  `json:"idOrdenTrabajo"`
	FechaRecojo        string `json:"fechaRecojo"`
	Mensaje            string `json:"mensaje"`
	FechaSolicitada    string `json:"fechaSolicitada"`
	MotivoPostergacion string `json:"motivoPostergacion"`
	HoraSolicitada     string `json:"horaSolicitada"`
	IdMotivoResultado  int64
}

const ordenTrabajoEsquema string = `CREATE TABLE x_taordentrabajo (
	idOrdenTrabajo int(11) NOT NULL AUTO_INCREMENT,
	fechaRecojo datetime DEFAULT NULL,
	mensaje varchar(200) DEFAULT NULL,
	fechaSolicitada datetime DEFAULT NULL,
	motivoPostergacion varchar(200) DEFAULT NULL,
	horaSolicitada datetime DEFAULT NULL,
	idMotivoResultado int(11) DEFAULT NULL,
	PRIMARY KEY (idOrdenTrabajo)
  );`

type OrdenTrabajos []OrdenTrabajo

func ListarOrdenTrabajos() OrdenTrabajos {
	sql := `SELECT idOrdenTrabajo, 
					fechaRecojo, 
					mensaje, 
					fechaSolicitada, 
					motivoPostergacion, 
					horaSolicitada, 
					idMotivoResultado 
					FROM x_taordentrabajo`
	listado := OrdenTrabajos{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := OrdenTrabajo{}
		rows.Scan(&obj.IdOrdenTrabajo,
			&obj.FechaRecojo,
			&obj.Mensaje,
			&obj.FechaSolicitada,
			&obj.MotivoPostergacion,
			&obj.HoraSolicitada,
			&obj.IdMotivoResultado)
		listado = append(listado, obj)
	}
	return listado
}

func ListarOrdenTrabajo(idOrdenTrabajo string) OrdenTrabajos {
	sql := `SELECT idOrdenTrabajo, 
					fechaRecojo, 
					mensaje, 
					fechaSolicitada, 
					motivoPostergacion, 
					horaSolicitada, 
					idMotivoResultado 
					FROM x_taordentrabajo where idOrdenTrabajo=?`
	listado := OrdenTrabajos{}
	rows, _ := Query(sql, idOrdenTrabajo)
	for rows.Next() {
		obj := OrdenTrabajo{}
		rows.Scan(&obj.IdOrdenTrabajo,
			&obj.FechaRecojo,
			&obj.Mensaje,
			&obj.FechaSolicitada,
			&obj.MotivoPostergacion,
			&obj.HoraSolicitada,
			&obj.IdMotivoResultado)
		listado = append(listado, obj)
	}
	return listado
}

func NuevaOrdenTrabajo(
	fechaRecojo,
	mensaje,
	fechaSolicitada,
	motivoPostergacion,
	horaSolicitada string,
	idMotivoResultado int64) *OrdenTrabajo {
	obj := &OrdenTrabajo{
		FechaRecojo:        fechaRecojo,
		Mensaje:            mensaje,
		FechaSolicitada:    fechaSolicitada,
		MotivoPostergacion: motivoPostergacion,
		HoraSolicitada:     horaSolicitada,
		IdMotivoResultado:  idMotivoResultado,
	}
	return obj
}

func CrearOrdenTrabajo(
	fechaRecojo,
	mensaje,
	fechaSolicitada,
	motivoPostergacion,
	horaSolicitada string,
	idMotivoResultado int64) *OrdenTrabajo {
	obj := NuevaOrdenTrabajo(
		fechaRecojo,
		mensaje,
		fechaSolicitada,
		motivoPostergacion,
		horaSolicitada,
		idMotivoResultado)
	obj.Guardar()
	return obj
}

func (this *OrdenTrabajo) Guardar() {
	if this.IdOrdenTrabajo == 0 {
		this.insertar()
	}
}

func (this *OrdenTrabajo) insertar() {
	sql := `INSERT x_taordentrabajo SET fechaRecojo=?, 
										mensaje=?, 
										fechaSolicitada=?, 
										motivoPostergacion=?, 
										horaSolicitada=?, 
										idMotivoResultado=?`
	fechaRec, _ := time.Parse(time.RFC3339, this.FechaRecojo)
	fechaSol, _ := time.Parse(time.RFC3339, this.FechaSolicitada)
	horaSol, _ := time.Parse(time.RFC3339, this.HoraSolicitada)
	result, err := Exec(sql,
		fechaRec,
		this.Mensaje,
		fechaSol,
		this.MotivoPostergacion,
		horaSol,
		this.IdMotivoResultado)
	this.IdOrdenTrabajo, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdOrdenTrabajo)
	}
}
