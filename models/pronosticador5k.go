package models

import (
	"time"
)

type Pronostico5k struct {
	IdPronostico5k int64  `json:"idPronostico5k"`
	Placa          string `json:"placa"`
	Modelo         string `json:"modelo"`
	Cliente        string `json:"cliente"`
	Telefono1      string `json:"telefono1"`
	Telefono2      string `json:"telefono2"`
	Mantenimiento  string `json:"mantenimiento"`
	Prioridad      string `json:"prioridad"`
	// ultimos agregados para pronostico
	KmPenultimoServicio     int64   `json:"kmPenultimoServicio"`
	KmUltimoServicio        int64   `json:"kmUltimoServicio"`
	KmRestoUP               int64   `json:"kmRestoUP"`
	ManttoToca              float64 `json:"manttoToca"`
	FechaPenultimoServicio  string  `json:"fechaPenultimoServicio"`
	FechaUltimoServicio     string  `json:"fechaUltimoServicio"`
	RestoFechasUP           int64   `json:"restoFechasUP"`
	KmPromedioFechaServicio float64 `json:"kmPromedioFechaServicio"`
	FaltaManttoToca         int64   `json:"faltaManttoToca"`
	CantDiasParaMantto      float64 `json:"cantDiasParaMantto"`
	Pronostico              string  `json:"pronostico"`
	// ant
	Fecha15antes               string `json:"fecha15antes"`
	FechaPactadaUltimoContacto string `json:"fechaPactadaUltimoContacto"`
	FechaCorregida             string `json:"fechaCorregida"`
	FechaCorregidaSistema      string `json:"fechaCorregidaSistema"`
	FechaWppEnvio              string `json:"fechaWppEnvio"`
	Vistobueno                 string `json:"vistobueno"`
	Resultado                  string `json:"resultado"`
	FechaLlamadaEfectuada      string `json:"fechaLlamadaEfectuada"`
	Contesta                   string `json:"contesta"`
	Resultado2                 string `json:"resultado2"`
	FechaPactadaVolverLlamar   string `json:"fechaPactadaVolverLlamar"`
	FechaCita                  string `json:"fechaCita"`
	Recordatorio               string `json:"recordatorio"`
	FechaRegistro              string `json:"fechaRegistro"`

	// nuevo campos requeridos
	NroPresupuesto string  `json:"nroPresupuesto"`
	Recomendacion  string  `json:"recomendacion"`
	Costo          float64 `json:"costo"`
}

const pronostico5kEsquema string = `CREATE TABLE x_tapronostico5k (
	idPronostico5k int(11) NOT NULL AUTO_INCREMENT,
	placa varchar(100) DEFAULT NULL,
	modelo varchar(100) DEFAULT NULL,
	cliente varchar(200) DEFAULT NULL,
	telefono1 varchar(100) DEFAULT NULL,
	telefono2 varchar(100) DEFAULT NULL,
	mantenimiento varchar(100) DEFAULT NULL,
	prioridad varchar(100) DEFAULT NULL,
	kmPenultimoServicio int(100) DEFAULT NULL,
	kmUltimoServicio int(100) DEFAULT NULL,
	kmRestoUP int(100) DEFAULT NULL,
	manttoToca int(100) DEFAULT NULL,
	fechaPenultimoServicio date DEFAULT NULL,
	fechaUltimoServicio date DEFAULT NULL,
	restoFechasUP int(100) DEFAULT NULL,
	kmPromedioFechaServicio float DEFAULT NULL,
	faltaManttoToca int(100) DEFAULT NULL,
	cantDiasParaMantto float DEFAULT NULL,
	pronostico date DEFAULT NULL,
	fecha15antes datetime DEFAULT NULL,
	fechaPactadaUltimoContacto datetime DEFAULT NULL,
	fechaCorregida datetime DEFAULT NULL,
	fechaCorregidaSistema datetime DEFAULT NULL,
	fechaWppEnvio datetime DEFAULT NULL,
	vistobueno varchar(100) DEFAULT NULL,
	resultado varchar(100) DEFAULT NULL,
	fechaLlamadaEfectuada datetime DEFAULT NULL,
	contesta varchar(100) DEFAULT NULL,
	resultado2 varchar(100) DEFAULT NULL,
	fechaPactadaVolverLlamar datetime DEFAULT NULL,
	fechaCita datetime DEFAULT NULL,
	recordatorio char(1) DEFAULT NULL,
	fechaRegistro datetime DEFAULT NULL,
	PRIMARY KEY (idPronostico5k)
  );
  `

type Pronostico5ks []Pronostico5k

func Pronostico5kLista() Pronostico5ks {
	sql := `SELECT x_tapronostico5k.idPronostico5k,
					x_tapronostico5k.placa,
					x_tapronostico5k.modelo,
					x_tapronostico5k.cliente,
					x_tapronostico5k.telefono1,
					x_tapronostico5k.telefono2,
					x_tapronostico5k.mantenimiento,
					x_tapronostico5k.prioridad,
					x_tapronostico5k.fecha15antes,
					x_tapronostico5k.fechaPactadaUltimoContacto,
					x_tapronostico5k.fechaCorregida,
					x_tapronostico5k.fechaCorregidaSistema,
					x_tapronostico5k.fechaWppEnvio,
					x_tapronostico5k.vistobueno,
					x_tapronostico5k.resultado,
					x_tapronostico5k.fechaLlamadaEfectuada,
					x_tapronostico5k.contesta,
					x_tapronostico5k.resultado2,
					x_tapronostico5k.fechaPactadaVolverLlamar,
					x_tapronostico5k.fechaCita,
					x_tapronostico5k.recordatorio,
					x_tapronostico5k.fechaRegistro
				FROM galaxia_corasur.x_tapronostico5k;`
	listado := Pronostico5ks{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Pronostico5k{}
		rows.Scan(
			&obj.IdPronostico5k,
			&obj.Placa,
			&obj.Modelo,
			&obj.Cliente,
			&obj.Telefono1,
			&obj.Telefono2,
			&obj.Mantenimiento,
			&obj.Prioridad,
			/*
				&obj.Prioridad,
				&obj.Prioridad,
				&obj.Prioridad,
				&obj.Prioridad,
				&obj.Prioridad,
				&obj.Prioridad,
				&obj.Prioridad,
				&obj.Prioridad,
				&obj.Prioridad,
				&obj.Prioridad,
				&obj.Prioridad,

			*/

			&obj.Fecha15antes,
			&obj.FechaPactadaUltimoContacto,
			&obj.FechaCorregida,
			&obj.FechaCorregidaSistema,
			&obj.FechaWppEnvio,
			&obj.Vistobueno,
			&obj.Resultado,
			&obj.FechaLlamadaEfectuada,
			&obj.Contesta,
			&obj.Resultado2,
			&obj.FechaPactadaVolverLlamar,
			&obj.FechaCita,
			&obj.Recordatorio,
			&obj.FechaRegistro)
		listado = append(listado, obj)
	}
	return listado
}

func Pronostico5kListaParametro(placa string) Pronostico5ks {
	sql := `SELECT x_tapronostico5k.idPronostico5k,
					x_tapronostico5k.placa,
					x_tapronostico5k.modelo,
					x_tapronostico5k.cliente,
					x_tapronostico5k.telefono1,
					x_tapronostico5k.telefono2,
					x_tapronostico5k.mantenimiento,
					x_tapronostico5k.prioridad,
					x_tapronostico5k.fecha15antes,
					x_tapronostico5k.fechaPactadaUltimoContacto,
					x_tapronostico5k.fechaCorregida,
					x_tapronostico5k.fechaCorregidaSistema,
					x_tapronostico5k.fechaWppEnvio,
					x_tapronostico5k.vistobueno,
					x_tapronostico5k.resultado,
					x_tapronostico5k.fechaLlamadaEfectuada,
					x_tapronostico5k.contesta,
					x_tapronostico5k.resultado2,
					x_tapronostico5k.fechaPactadaVolverLlamar,
					x_tapronostico5k.fechaCita,
					x_tapronostico5k.recordatorio,
					x_tapronostico5k.fechaRegistro
				FROM galaxia_corasur.x_tapronostico5k where placa=?;`
	listado := Pronostico5ks{}
	rows, _ := Query(sql, placa)
	for rows.Next() {
		obj := Pronostico5k{}
		rows.Scan(
			&obj.IdPronostico5k,
			&obj.Placa,
			&obj.Modelo,
			&obj.Cliente,
			&obj.Telefono1,
			&obj.Telefono2,
			&obj.Mantenimiento,
			&obj.Prioridad,
			&obj.Fecha15antes,
			&obj.FechaPactadaUltimoContacto,
			&obj.FechaCorregida,
			&obj.FechaCorregidaSistema,
			&obj.FechaWppEnvio,
			&obj.Vistobueno,
			&obj.Resultado,
			&obj.FechaLlamadaEfectuada,
			&obj.Contesta,
			&obj.Resultado2,
			&obj.FechaPactadaVolverLlamar,
			&obj.FechaCita,
			&obj.Recordatorio,
			&obj.FechaRegistro)
		listado = append(listado, obj)
	}
	return listado
}

func NuevoPronostico5k(
	placa,
	modelo,
	cliente,
	telefono1,
	telefono2,
	mantenimiento,
	prioridad string,
	kmPenultimoServicio,
	kmUltimoServicio,
	kmRestoUP int64,
	manttoToca float64,
	fechaPenultimoServicio,
	fechaUltimoServicio string,
	restoFechasUP int64,
	kmPromedioFechaServicio float64,
	faltaManttoToca int64,
	cantDiasParaMantto float64,
	pronostico,
	fecha15antes,
	fechaPactadaUltimoContacto,
	fechaCorregida,
	fechaCorregidaSistema,
	fechaWppEnvio,
	vistobueno,
	resultado,
	fechaLlamadaEfectuada,
	contesta,
	resultado2,
	fechaPactadaVolverLlamar,
	fechaCita,
	recordatorio,
	fechaRegistro,
	nroPresupuesto,
	recomendacion string,
	costo float64) *Pronostico5k {
	obj := &Pronostico5k{
		Placa:         placa,
		Modelo:        modelo,
		Cliente:       cliente,
		Telefono1:     telefono1,
		Telefono2:     telefono2,
		Mantenimiento: mantenimiento,
		Prioridad:     prioridad,
		// new
		KmPenultimoServicio:     kmPenultimoServicio,
		KmUltimoServicio:        kmUltimoServicio,
		KmRestoUP:               kmRestoUP,
		ManttoToca:              manttoToca,
		FechaPenultimoServicio:  fechaPenultimoServicio,
		FechaUltimoServicio:     fechaUltimoServicio,
		RestoFechasUP:           restoFechasUP,
		KmPromedioFechaServicio: kmPromedioFechaServicio,
		FaltaManttoToca:         faltaManttoToca,
		CantDiasParaMantto:      cantDiasParaMantto,
		Pronostico:              pronostico,
		// det
		Fecha15antes:               fecha15antes,
		FechaPactadaUltimoContacto: fechaPactadaUltimoContacto,
		FechaCorregida:             fechaCorregida,
		FechaCorregidaSistema:      fechaCorregidaSistema,
		FechaWppEnvio:              fechaWppEnvio,
		Vistobueno:                 vistobueno,
		Resultado:                  resultado,
		FechaLlamadaEfectuada:      fechaLlamadaEfectuada,
		Contesta:                   contesta,
		Resultado2:                 resultado2,
		FechaPactadaVolverLlamar:   fechaPactadaVolverLlamar,
		FechaCita:                  fechaCita,
		Recordatorio:               recordatorio,
		FechaRegistro:              fechaRegistro,
		// ultimos solicitados
		NroPresupuesto: nroPresupuesto,
		Recomendacion:  recomendacion,
		Costo:          costo,
	}
	return obj
}

func CrearPronostico5k(
	placa,
	modelo,
	cliente,
	telefono1,
	telefono2,
	mantenimiento,
	prioridad string,
	kmPenultimoServicio,
	kmUltimoServicio,
	kmRestoUP int64,
	manttoToca float64,
	fechaPenultimoServicio,
	fechaUltimoServicio string,
	restoFechasUP int64,
	kmPromedioFechaServicio float64,
	faltaManttoToca int64,
	cantDiasParaMantto float64,
	pronostico,
	fecha15antes,
	fechaPactadaUltimoContacto,
	fechaCorregida,
	fechaCorregidaSistema,
	fechaWppEnvio,
	vistobueno,
	resultado,
	fechaLlamadaEfectuada,
	contesta,
	resultado2,
	fechaPactadaVolverLlamar,
	fechaCita,
	recordatorio,
	fechaRegistro,
	nroPresupuesto,
	recomendacion string,
	costo float64) *Pronostico5k {
	obj := NuevoPronostico5k(
		placa,
		modelo,
		cliente,
		telefono1,
		telefono2,
		mantenimiento,
		prioridad,
		kmPenultimoServicio,
		kmUltimoServicio,
		kmRestoUP,
		manttoToca,
		fechaPenultimoServicio,
		fechaUltimoServicio,
		restoFechasUP,
		kmPromedioFechaServicio,
		faltaManttoToca,
		cantDiasParaMantto,
		pronostico,
		fecha15antes,
		fechaPactadaUltimoContacto,
		fechaCorregida,
		fechaCorregidaSistema,
		fechaWppEnvio,
		vistobueno,
		resultado,
		fechaLlamadaEfectuada,
		contesta,
		resultado2,
		fechaPactadaVolverLlamar,
		fechaCita,
		recordatorio,
		fechaRegistro,
		nroPresupuesto,
		recomendacion,
		costo)
	obj.Guardar()
	return obj
}

func (this *Pronostico5k) Guardar() {
	if this.IdPronostico5k == 0 {
		this.insertar()
	} else {
		this.actualizar()
	}
}

func (this *Pronostico5k) insertar() {
	sql := `INSERT INTO galaxia_corasur.x_tapronostico5k SET placa=?,
										modelo=?,
										cliente=?,
										telefono1=?,
										telefono2=?,
										mantenimiento=?,
										prioridad=?,
										kmPenultimoServicio=?,
										kmUltimoServicio=?,
										kmRestoUP=?,
										manttoToca=?,
										fechaPenultimoServicio=?,
										fechaUltimoServicio=?,
										restoFechasUP=?,
										kmPromedioFechaServicio=?,
										faltaManttoToca=?,
										cantDiasParaMantto=?,
										pronostico=?,
										fecha15antes=?,
										fechaPactadaUltimoContacto=?,
										fechaCorregida=?,
										fechaCorregidaSistema=?,
										fechaWppEnvio=?,
										vistobueno=?,
										resultado=?,
										fechaLlamadaEfectuada=?,
										contesta=?,
										resultado2=?,
										fechaPactadaVolverLlamar=?,
										fechaCita=?,
										recordatorio=?,
										fechaRegistro=?,
										nroPresupuesto=?,
										recomendacion=?,
										costo=?`

	fechPS, _ := time.Parse(time.RFC3339, this.FechaPenultimoServicio)
	fechUS, _ := time.Parse(time.RFC3339, this.FechaUltimoServicio)
	// pronostico, _ := time.Parse(time.RFC3339, this.Pronostico)
	// fecha15, _ := time.Parse(time.RFC3339, this.Fecha15antes)
	fechaPacUC, _ := time.Parse(time.RFC3339, this.FechaPactadaUltimoContacto)
	fechaC, _ := time.Parse(time.RFC3339, this.FechaCorregida)
	fechaCS, _ := time.Parse(time.RFC3339, this.FechaCorregidaSistema)
	fechaWpp, _ := time.Parse(time.RFC3339, this.FechaWppEnvio)
	fechaLE, _ := time.Parse(time.RFC3339, this.FechaLlamadaEfectuada)
	fechaPVL, _ := time.Parse(time.RFC3339, this.FechaPactadaVolverLlamar)
	fechaCi, _ := time.Parse(time.RFC3339, this.FechaCita)
	fechaR, _ := time.Parse(time.RFC3339, this.FechaRegistro)
	result, err := Exec(sql,
		this.Placa,
		this.Modelo,
		this.Cliente,
		this.Telefono1,
		this.Telefono2,
		this.Mantenimiento,
		this.Prioridad,
		// ultimos agregados
		this.KmPenultimoServicio,
		this.KmUltimoServicio,
		this.KmRestoUP,
		this.ManttoToca,
		fechPS,
		fechUS,
		this.RestoFechasUP,
		this.KmPromedioFechaServicio,
		this.FaltaManttoToca,
		this.CantDiasParaMantto,
		this.Pronostico,
		// det
		this.Fecha15antes,
		fechaPacUC,
		fechaC,
		fechaCS,
		fechaWpp,
		this.Vistobueno,
		this.Resultado,
		fechaLE,
		this.Contesta,
		this.Resultado2,
		fechaPVL,
		fechaCi,
		this.Recordatorio,
		fechaR,
		this.NroPresupuesto,
		this.Recomendacion,
		this.Costo)
	this.IdPronostico5k, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdPronostico5k)
	}
}

func (this *Pronostico5k) actualizar() {
	sql := `UPDATE galaxia_corasur.x_tapronostico5k SET fechaLlamadaEfectuada=?,
										contesta=?,
										resultado2=?,
										fechaPactadaVolverLlamar=?,
										fechaCita=? WHERE IdPronostico5k=?`
	fechaLE, _ := time.Parse(time.RFC3339, this.FechaLlamadaEfectuada)
	fechaPVL, _ := time.Parse(time.RFC3339, this.FechaPactadaVolverLlamar)
	fechaCi, _ := time.Parse(time.RFC3339, this.FechaCita)

	Exec(sql,
		fechaLE,
		this.Contesta,
		this.Resultado2,
		fechaPVL,
		fechaCi,
		this.IdPronostico5k)

}
func ActualizarPro(id int64, fechallamada, contesta, resultado2, fechapvl, fechaci string) {
	sql := `UPDATE galaxia_corasur.x_tapronostico5k SET fechaLlamadaEfectuada=?,
												contesta=?,
												resultado2=?,
												fechaPactadaVolverLlamar=?,
												fechaCita=? WHERE IdPronostico5k=?`
	fechaLE, _ := time.Parse(time.RFC3339, fechallamada)
	fechaPVL, _ := time.Parse(time.RFC3339, fechapvl)
	fechaCi, _ := time.Parse(time.RFC3339, fechaci)

	Exec(sql,
		fechaLE,
		contesta,
		resultado2,
		fechaPVL,
		fechaCi,
		id)
}
