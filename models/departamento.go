package models

type Departamento struct {
	IdDepartamento int64  `json:"idDepartamento"`
	Departamento   string `json:"departamento"`
}

const departamentoEsquema string = `CREATE TABLE x_tadepartamento (
	idDepartamento int(11) NOT NULL AUTO_INCREMENT,
	departamento varchar(200) NULL,
	PRIMARY KEY (idDepartamento)
  );`

type Departamentos []Departamento

// constructores
func ListarDepartamentos() Departamentos {
	sql := "SELECT idDepartamento, departamento FROM x_tadepartamento"
	listado := Departamentos{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Departamento{}
		rows.Scan(&obj.IdDepartamento, &obj.Departamento)
		listado = append(listado, obj)
	}
	return listado
}

func ListarDepartamento(departamento string) Departamentos {
	sql := "SELECT idDepartamento, departamento FROM x_tadepartamento WHERE departamento=?"
	listado := Departamentos{}
	rows, _ := Query(sql, departamento)
	for rows.Next() {
		obj := Departamento{}
		rows.Scan(&obj.IdDepartamento, &obj.Departamento)
		listado = append(listado, obj)
	}
	return listado
}
