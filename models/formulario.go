package models

type Formulario struct {
	Contacto      Contacto      `json:"contacto"`
	Cita          []Cita        `json:"cita"`
	NoCita        NoCita        `json:"nocita"`
	Vehiculo      Vehiculo      `json:"vehiculo"`
	Presupuesto   Presupuesto   `json:"presupuesto"`
	OrdenTrabajo  OrdenTrabajo  `json:"ordenTrabajo"`
	Postergacion  Postergacion  `json:"postergacion"`
	Encuesta      Encuesta      `json:"encuesta"`
	Seguimiento   Seguimiento   `json:"seguimiento"`
	Recomendacion Recomendacion `json:"recomendacion"`
	Pronostico5k  Pronostico5k  `json:"pronostico5k"`
}
