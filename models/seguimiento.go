package models

import "time"

type Seguimiento struct {
	IdSeguimiento    int64  `json:"idSeguimiento"`
	VozCliente       string `json:"vozCliente"`
	AlertaTDP        string `json:"alertaTDP"`
	ResultadoAlerta  string `json:"resultadoAlerta"`
	AtencionAlerta   string `json:"atencionAlerta"`
	DetalleAtencion  string `json:"detalleAtencion"`
	FechaCitaRetorno string `json:"fechaCitaRetorno"`
	TipoDescuento    string `json:"tipoDescuento"`
	Caducidad        string `json:"caducidad"`
	TipoServicio     string `json:"tipoServicio"`
	Servicio         string `json:"servicio"`
	Otro             string `json:"otro"`
}

const seguimientoEsquema string = `CREATE TABLE x_taseguimiento (
	idSeguimiento int(11) NOT NULL AUTO_INCREMENT,
	vozCliente varchar(100) NULL,  
	alertaTDP varchar(100) NULL,  
	resultadoAlerta varchar(100) NULL,  
	atencionAlerta varchar(100) NULL,  
	detalleAtencion varchar(100) NULL,  
	fechaCitaRetorno datetime,
	tipoDescuento varchar(100) NULL,  
	caducidad varchar(100) NULL,  
	tipoServicio varchar(100) NULL,  
	servicio varchar(100) NULL,  
	otro varchar(100) NULL,
	PRIMARY KEY (idSeguimiento)
  );`

type Seguimientos []Seguimiento

func ListarSeguimientos() Seguimientos {
	sql := `SELECT idSeguimiento, 
				vozCliente, 
				alertaTDP, 
				resultadoAlerta, 
				atencionAlerta, 
				detalleAtencion, 
				fechaCitaRetorno, 
				tipoDescuento, 
				caducidad, 
				tipoServicio, 
				servicio,
				otro FROM x_taseguimiento`
	listado := Seguimientos{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Seguimiento{}
		rows.Scan(&obj.IdSeguimiento,
			&obj.VozCliente,
			&obj.AlertaTDP,
			&obj.ResultadoAlerta,
			&obj.AtencionAlerta,
			&obj.DetalleAtencion,
			&obj.FechaCitaRetorno,
			&obj.TipoDescuento,
			&obj.Caducidad,
			&obj.TipoServicio,
			&obj.Servicio,
			&obj.Otro)
		listado = append(listado, obj)
	}
	return listado
}

func ListarSeguimiento(idSeguimiento string) Seguimientos {
	sql := `SELECT idSeguimiento, 
				vozCliente, 
				alertaTDP, 
				resultadoAlerta, 
				atencionAlerta, 
				detalleAtencion, 
				fechaCitaRetorno, 
				tipoDescuento, 
				caducidad, 
				tipoServicio, 
				servicio,
				otro FROM x_taseguimiento where idSeguimiento=?`
	listado := Seguimientos{}
	rows, _ := Query(sql, idSeguimiento)
	for rows.Next() {
		obj := Seguimiento{}
		rows.Scan(&obj.IdSeguimiento,
			&obj.VozCliente,
			&obj.AlertaTDP,
			&obj.ResultadoAlerta,
			&obj.AtencionAlerta,
			&obj.DetalleAtencion,
			&obj.FechaCitaRetorno,
			&obj.TipoDescuento,
			&obj.Caducidad,
			&obj.TipoServicio,
			&obj.Servicio,
			&obj.Otro)
		listado = append(listado, obj)
	}
	return listado
}

func NuevoSeguimiento(
	vozCliente,
	alertaTDP,
	resultadoAlerta,
	atencionAlerta,
	detalleAtencion,
	fechaCitaRetorno,
	tipoDescuento,
	caducidad,
	tipoServicio,
	servicio,
	otro string) *Seguimiento {
	obj := &Seguimiento{
		VozCliente:       vozCliente,
		AlertaTDP:        alertaTDP,
		ResultadoAlerta:  resultadoAlerta,
		AtencionAlerta:   atencionAlerta,
		DetalleAtencion:  detalleAtencion,
		FechaCitaRetorno: fechaCitaRetorno,
		TipoDescuento:    tipoDescuento,
		Caducidad:        caducidad,
		TipoServicio:     tipoServicio,
		Servicio:         servicio,
		Otro:             otro,
	}
	return obj
}

func CrearSeguimiento(
	vozCliente,
	alertaTDP,
	resultadoAlerta,
	atencionAlerta,
	detalleAtencion,
	fechaCitaRetorno,
	tipoDescuento,
	caducidad,
	tipoServicio,
	servicio,
	otro string) *Seguimiento {
	obj := NuevoSeguimiento(
		vozCliente,
		alertaTDP,
		resultadoAlerta,
		atencionAlerta,
		detalleAtencion,
		fechaCitaRetorno,
		tipoDescuento,
		caducidad,
		tipoServicio,
		servicio,
		otro)
	obj.Guardar()
	return obj
}

func (this *Seguimiento) Guardar() {
	if this.IdSeguimiento == 0 {
		this.insertar()
	}
}

func (this *Seguimiento) insertar() {
	sql := `INSERT x_taseguimiento SET vozCliente=?, 
											alertaTDP=?, 
											resultadoAlerta=?, 
											atencionAlerta=?, 
											detalleAtencion=?, 
											fechaCitaRetorno=?, 
											tipoDescuento=?, 
											caducidad=?, 
											tipoServicio=?, 
											servicio=?,
											otro=?`
	fechaCR, _ := time.Parse(time.RFC3339, this.FechaCitaRetorno)
	result, err := Exec(sql,
		this.VozCliente,
		this.AlertaTDP,
		this.ResultadoAlerta,
		this.AtencionAlerta,
		this.DetalleAtencion,
		fechaCR,
		this.TipoDescuento,
		this.Caducidad,
		this.TipoServicio,
		this.Servicio,
		this.Otro)
	this.IdSeguimiento, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdSeguimiento)
	}
}
