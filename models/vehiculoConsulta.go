package models

type VehiculoConsulta struct {
	Serie           string `json:"serie"`
	Motor           string `json:"motor"`
	Chasis          string `json:"chasis"`
	Vin             string `json:"vin"`
	AnioFabricacion string `json:"anioFabricacion"`
	Modelo          string `json:"modelo"`
}

type VehiculoConsultas []VehiculoConsulta

func ListarVehiculosConsulta() VehiculoConsultas {
	sql := `select 	c26 as serie,        
					c8 as motor,
					c25 as chasis,
					c7 as vin,             
					c5 as anioFabricacion,
					c3 as modelo                    
				from tx_salidac where canudocu='N'`
	listado := VehiculoConsultas{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := VehiculoConsulta{}
		rows.Scan(&obj.Serie, &obj.Motor, &obj.Chasis, &obj.Vin, &obj.AnioFabricacion, &obj.Modelo)
		listado = append(listado, obj)
	}
	return listado
}

func ListarVehiculoConsulta(tdp string) VehiculoConsultas {
	sql := `select 	c26 as serie,        
					c8 as motor,
					c25 as chasis,
					c7 as vin,             
					c5 as anioFabricacion,
					c3 as modelo                    
				from tx_salidac where canudocu='N' and impreso='V' and c26=?`
	listado := VehiculoConsultas{}
	rows, _ := Query(sql, tdp)
	for rows.Next() {
		obj := VehiculoConsulta{}
		rows.Scan(&obj.Serie, &obj.Motor, &obj.Chasis, &obj.Vin, &obj.AnioFabricacion, &obj.Modelo)
		listado = append(listado, obj)
	}
	return listado
}
