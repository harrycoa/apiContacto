package models

import "time"

type VehiculoEntrega struct {
	IdVehiculoEntrega        int64  `json:"idVehiculoEntrega"`
	FechaProgramacionEntrega string `json:"fechaProgramacionEntrega"`
	FechaEntregaVehiculo     string `json:"fechaEntregaVehiculo"`
	PersonaEntrega           string `json:"personaEntrega"`
	Telefono                 string `json:"telefono"`
	Celular                  string `json:"celular"`
	OpinionEntrega           string `json:"opinionEntrega"`
	FechaCitaReferencial2    string `json:"fechaCitaReferencial2"`
	IdVehiculo               int64
	IdVehiculoUso            int64
	IdDepartamento           int64
	IdProvincia              int64
}

const vehiculoEntregaEsquema string = `CREATE TABLE x_tavehiculoentrega (
	idVehiculoEntrega int(11) NOT NULL AUTO_INCREMENT,
	fechaEntregaVehiculo datetime DEFAULT NULL,
	personaEntrega varchar(200) DEFAULT NULL,
	telefono varchar(10) DEFAULT NULL,
	celular varchar(10) DEFAULT NULL,
	opinionEntrega varchar(200) DEFAULT NULL,
	fechaCitaReferencial2 datetime DEFAULT NULL,
	idVehiculo int(11) DEFAULT NULL,
	idVehiculoUso int(11) DEFAULT NULL,
	idDepartamento int(11) DEFAULT NULL,
	idProvincia int(11) DEFAULT NULL,
	PRIMARY KEY (idVehiculoEntrega)
  );`

type VehiculoEntregas []VehiculoEntrega

func ListarVehiculosEntrega() VehiculoEntregas {
	sql := `SELECT idVehiculoEntrega, 
				fechaEntregaVehiculo, 
				personaEntrega, 
				telefono, 
				celular, 
				opinionEntrega, 
				fechaCitaReferencial2, 
				idVehiculo, 
				idVehiculoUso, 
				idDepartamento, 
				idProvincia FROM x_tavehiculoentrega`
	listado := VehiculoEntregas{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := VehiculoEntrega{}
		rows.Scan(&obj.IdVehiculoEntrega,
			&obj.FechaProgramacionEntrega,
			&obj.FechaEntregaVehiculo,
			&obj.PersonaEntrega,
			&obj.Telefono,
			&obj.Celular,
			&obj.OpinionEntrega,
			&obj.FechaCitaReferencial2,
			&obj.IdVehiculo,
			&obj.IdVehiculoUso,
			&obj.IdDepartamento,
			&obj.IdProvincia)
		listado = append(listado, obj)
	}
	return listado
}

func ListarVehiculoEntrega(idVehiculoEntrega int64) VehiculoEntregas {
	sql := `SELECT idVehiculoEntrega, 
				fechaProgramacionEntrega,
				fechaEntregaVehiculo, 
				personaEntrega, 
				telefono, 
				celular, 
				opinionEntrega, 
				fechaCitaReferencial2, 
				idVehiculo, 
				idVehiculoUso, 
				idDepartamento, 
				idProvincia FROM x_tavehiculoentrega where idVehiculo=?`
	listado := VehiculoEntregas{}
	rows, _ := Query(sql, idVehiculoEntrega)
	for rows.Next() {
		obj := VehiculoEntrega{}
		rows.Scan(&obj.IdVehiculoEntrega,
			&obj.FechaProgramacionEntrega,
			&obj.FechaEntregaVehiculo,
			&obj.PersonaEntrega,
			&obj.Telefono,
			&obj.Celular,
			&obj.OpinionEntrega,
			&obj.FechaCitaReferencial2,
			&obj.IdVehiculo,
			&obj.IdVehiculoUso,
			&obj.IdDepartamento,
			&obj.IdProvincia)
		listado = append(listado, obj)
	}
	return listado
}

func NuevoVehiculoEntrega(
	fechaProgramacionEntrega,
	fechaEntregaVehiculo,
	personaEntrega,
	telefono,
	celular,
	opinionEntrega,
	fechaCitaReferencial2 string,
	idVehiculo,
	idVehiculoUso,
	idDepartamento,
	idProvincia int64) *VehiculoEntrega {
	objvehiculoentrega := &VehiculoEntrega{
		FechaProgramacionEntrega: fechaProgramacionEntrega,
		FechaEntregaVehiculo:     fechaEntregaVehiculo,
		PersonaEntrega:           personaEntrega,
		Telefono:                 telefono,
		Celular:                  celular,
		OpinionEntrega:           opinionEntrega,
		FechaCitaReferencial2:    fechaCitaReferencial2,
		IdVehiculo:               idVehiculo,
		IdVehiculoUso:            idVehiculoUso,
		IdDepartamento:           idDepartamento,
		IdProvincia:              idProvincia,
	}
	return objvehiculoentrega
}

func CrearVehiculoEntrega(
	fechaProgramacionEntrega,
	fechaEntregaVehiculo,
	personaEntrega,
	telefono,
	celular,
	opinionEntrega,
	fechaCitaReferencial2 string,
	idVehiculo,
	idVehiculoUso,
	idDepartamento,
	idProvincia int64) *VehiculoEntrega {
	objvehiculoE := NuevoVehiculoEntrega(
		fechaProgramacionEntrega,
		fechaEntregaVehiculo,
		personaEntrega,
		telefono,
		celular,
		opinionEntrega,
		fechaCitaReferencial2,
		idVehiculo,
		idVehiculoUso,
		idDepartamento,
		idProvincia)
	objvehiculoE.Guardar()
	return objvehiculoE
}

func (this *VehiculoEntrega) Guardar() {
	if this.IdVehiculoEntrega == 0 {
		this.insertar()
	}
}

func (this *VehiculoEntrega) insertar() {
	sql := `INSERT x_tavehiculoentrega SET fechaProgramacionEntrega=?,
											fechaEntregaVehiculo=?, 
											personaEntrega=?, 
											telefono=?, 
											celular=?, 
											opinionEntrega=?, 
											fechaCitaReferencial2=?, 
											idVehiculo=?, 
											idVehiculoUso=?, 
											idDepartamento=?, 
											idProvincia=?`
	fechaP, _ := time.Parse(time.RFC3339, this.FechaProgramacionEntrega)
	fechaE, _ := time.Parse(time.RFC3339, this.FechaEntregaVehiculo)
	fechaR2, _ := time.Parse(time.RFC3339, this.FechaCitaReferencial2)
	result, err := Exec(sql,
		fechaP,
		fechaE,
		this.PersonaEntrega,
		this.Telefono,
		this.Celular,
		this.OpinionEntrega,
		fechaR2,
		this.IdVehiculo,
		this.IdVehiculoUso,
		this.IdDepartamento,
		this.IdProvincia)
	this.IdVehiculoEntrega, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdVehiculoEntrega)
	}
}
