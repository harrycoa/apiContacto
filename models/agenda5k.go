package models

type Agenda5k struct {
	IdPronostico5k             int64   `json:"idPronostico5k"`
	Placa                      string  `json:"placa"`
	Modelo                     string  `json:"modelo"`
	Cliente                    string  `json:"cliente"`
	Telefono1                  string  `json:"telefono1"`
	Telefono2                  string  `json:"telefono2"`
	Mantenimiento              string  `json:"mantenimiento"`
	Prioridad                  string  `json:"prioridad"`
	Fecha15Antes               string  `json:"fecha15Antes"`
	FechaPactadaUltimoContacto string  `json:"fechaPactadaUltimoContacto"`
	FechaCorregida             string  `json:"fechaCorregida"`
	FechaCorregidaSistema      string  `json:"fechaCorregidaSistema"`
	FechaWppEnvio              string  `json:"fechaWppEnvio"`
	VistoBueno                 string  `json:"vistoBueno"`
	Resultado                  string  `json:"resultado"`
	FechaLlamadaEfectuada      string  `json:"fechaLlamadaEfectuada"`
	Contesta                   string  `json:"contesta"`
	Resultado2                 string  `json:"resultado2"`
	FechaPactadaVolverLlamar   string  `json:"fechaPactadaVolverLlamar"`
	FechaCita                  string  `json:"fechaCita"`
	NroPrepuesto               string  `json:"nroPresupuesto"`
	Recomendacion              string  `json:"recomendacion"`
	Costo                      float64 `json:"costo"`
}

type Agenda5ks []Agenda5k

func ListarAgenda5k() Agenda5ks {
	sql := `SELECT galaxia_corasur.x_tapronostico5k.idPronostico5k,
	galaxia_corasur.x_tapronostico5k.placa,
	galaxia_corasur.x_tapronostico5k.modelo,
	galaxia_corasur.x_tapronostico5k.cliente,
	galaxia_corasur.x_tapronostico5k.telefono1,
	galaxia_corasur.x_tapronostico5k.telefono1,
	galaxia_corasur.x_tapronostico5k.manttoToca,
	galaxia_corasur.x_tapronostico5k.prioridad,
	galaxia_corasur.x_tapronostico5k.fecha15antes,
	galaxia_corasur.x_tapronostico5k.fechaPactadaUltimoContacto,
	galaxia_corasur.x_tapronostico5k.fechaCorregida,
	galaxia_corasur.x_tapronostico5k.fecha15antes,
	galaxia_corasur.x_tapronostico5k.fechaWppEnvio,
	galaxia_corasur.x_tapronostico5k.vistobueno,
	galaxia_corasur.x_tapronostico5k.resultado2,
	galaxia_corasur.x_tapronostico5k.fechaPactadaVolverLlamar,
	galaxia_corasur.x_tapronostico5k.contesta,
	galaxia_corasur.x_tapronostico5k.resultado,
	galaxia_corasur.x_tapronostico5k.fechaPactadaVolverLlamar,
	galaxia_corasur.x_tapronostico5k.fechaCita,
	galaxia_corasur.x_tapronostico5k.nroPresupuesto,
	galaxia_corasur.x_tapronostico5k.recomendacion,
	galaxia_corasur.x_tapronostico5k.costo       
FROM galaxia_corasur.x_tapronostico5k where galaxia_corasur.x_tapronostico5k.fecha15antes > '20183011'`
	listado := Agenda5ks{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := Agenda5k{}
		rows.Scan(
			&obj.IdPronostico5k,
			&obj.Placa,
			&obj.Modelo,
			&obj.Cliente,
			&obj.Telefono1,
			&obj.Telefono2,
			&obj.Mantenimiento,
			&obj.Prioridad,
			&obj.Fecha15Antes,
			&obj.FechaPactadaUltimoContacto,
			&obj.FechaCorregida,
			&obj.FechaCorregidaSistema,
			&obj.FechaWppEnvio,
			&obj.VistoBueno,
			&obj.Resultado,
			&obj.FechaLlamadaEfectuada,
			&obj.Contesta,
			&obj.Resultado2,
			&obj.FechaPactadaVolverLlamar,
			&obj.FechaCita,
			&obj.NroPrepuesto,
			&obj.Recomendacion,
			&obj.Costo)
		listado = append(listado, obj)
	}
	return listado
}

func ListarAgenda5kParametro(fecha1 string, fecha2 string) Agenda5ks {
	sql := `SELECT galaxia_corasur.x_tapronostico5k.idPronostico5k,
	galaxia_corasur.x_tapronostico5k.placa,
	galaxia_corasur.x_tapronostico5k.modelo,
	galaxia_corasur.x_tapronostico5k.cliente,
	galaxia_corasur.x_tapronostico5k.telefono1,
	galaxia_corasur.x_tacontacto.contactoCliente,
	galaxia_corasur.x_tapronostico5k.manttoToca,
	galaxia_corasur.x_tapronostico5k.prioridad,
	galaxia_corasur.x_tapronostico5k.fecha15antes,
	galaxia_corasur.x_tacontacto.fechaRetomarContacto,
	galaxia_corasur.x_tapronostico5k.fecha15antes,
	galaxia_corasur.x_tapronostico5k.fecha15antes,
	galaxia_corasur.x_tapronostico5k.fechaWppEnvio,
	galaxia_corasur.x_tapronostico5k.vistobueno,
	galaxia_corasur.x_tacontacto.resultadoContacto,
	galaxia_corasur.x_tacontacto.fechaRegistro,
	galaxia_corasur.x_tacontacto.activo,
	galaxia_corasur.x_tapronostico5k.resultado2,
	galaxia_corasur.x_tapronostico5k.fechaPactadaVolverLlamar,
	galaxia_corasur.x_tapronostico5k.fechaCita,
	galaxia_corasur.x_tapronostico5k.nroPresupuesto,
	galaxia_corasur.x_tapronostico5k.recomendacion,
	galaxia_corasur.x_tapronostico5k.costo       
FROM galaxia_corasur.x_tapronostico5k inner join galaxia_corasur.x_tacontacto on galaxia_corasur.x_tapronostico5k.placa=galaxia_corasur.x_tacontacto.placa
where galaxia_corasur.x_tapronostico5k.kmPenultimoServicio > 0 and 
galaxia_corasur.x_tapronostico5k.kmRestoUP > 0 and fe
group by galaxia_corasur.x_tapronostico5k.idPronostico5k 
order by galaxia_corasur.x_tapronostico5k.pronostico asc;`
	listado := Agenda5ks{}
	rows, _ := Query(sql, fecha1, fecha2)
	for rows.Next() {
		obj := Agenda5k{}
		rows.Scan(
			&obj.IdPronostico5k,
			&obj.Placa,
			&obj.Modelo,
			&obj.Cliente,
			&obj.Telefono1,
			&obj.Telefono2,
			&obj.Mantenimiento,
			&obj.Prioridad,
			&obj.Fecha15Antes,
			&obj.FechaPactadaUltimoContacto,
			&obj.FechaCorregida,
			&obj.FechaCorregidaSistema,
			&obj.FechaWppEnvio,
			&obj.VistoBueno,
			&obj.Resultado,
			&obj.FechaLlamadaEfectuada,
			&obj.Contesta,
			&obj.Resultado2,
			&obj.FechaPactadaVolverLlamar,
			&obj.FechaCita,
			&obj.NroPrepuesto,
			&obj.Recomendacion,
			&obj.Costo)
		listado = append(listado, obj)
	}
	return listado
}
