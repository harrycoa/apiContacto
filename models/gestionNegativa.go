package models

import "time"

type GestionNegativa struct {
	IdGestionNegativa    int64  `json:"idGestionNegativa"`
	Placa                string `json:"placa"`
	Cliente              string `json:"cliente"`
	ContactoCliente      string `json:"contactoCliente"`
	Activo               string `json:"activo"`
	IdEstado             int64
	ResultadoContacto    string `json:"resultadoContacto"`
	FechaRetomarContacto string `json:"fechaRetomarContacto"`
	AtendidoGestion      string `json:"atendidoGestion"`
	IndicacionSugerida   string `json:"indicacionSugerida"`
	DetalleGestion       string `json:"detalleGestion"`
	FechaRetornoCorasur  string `json:"fechaRetornoCorasur"`
	TipoDescuento        string `json:"tipoDescuento"`
	Caducidad            string `json:"caducidad"`
	TipoServicio         string `json:"tipoServicio"`
	Servicio             string `json:"servicio"`
	FechaRegistro        string `json:"fechaRegistro"`
	IdContacto           int64
}

const gestionNegativaEsquema string = `CREATE TABLE x_tagestionnegativa (
	idGestionNegativa int(11) NOT NULL AUTO_INCREMENT,
	placa varchar(6) NULL,
	cliente varchar(200) NULL,  
	contactoCliente varchar(200) NULL,  
	activo char(1),
	idEstado varchar(100) NULL,  
	resultadoContacto int(11),
	fechaRetomarContacto datetime,
	atendidoGestion char(1),
	indicacionSugerida varchar(100) NULL,
	detalleGestion varchar(200) null,
	fechaRetornoCorasur datetime,
	tipoDescuento varchar(50) null,
	caducidad varchar(50) null,
	tipoServicio varchar(50) null,
	servicio varchar(50) null,
	idContacto int(11),
	PRIMARY KEY (idGestionVozCliente)
  );`

type GestionNegativas []GestionNegativa

func NuevaGestionNegativa(
	placa,
	cliente,
	contactoCliente,
	activo string,
	idEstado int64,
	resultadoContacto string,
	fechaRetomarContacto,
	atendidoGestion,
	indicacionSugerida,
	detalleGestion,
	fechaRetornoCorasur,
	tipoDescuento,
	caducidad,
	tipoServicio,
	servicio,
	fechaRegistro string,
	idContacto int64) *GestionNegativa {
	obj := &GestionNegativa{
		Placa:                placa,
		Cliente:              cliente,
		ContactoCliente:      contactoCliente,
		Activo:               activo,
		IdEstado:             idEstado,
		ResultadoContacto:    resultadoContacto,
		FechaRetomarContacto: fechaRetomarContacto,
		AtendidoGestion:      atendidoGestion,
		IndicacionSugerida:   indicacionSugerida,
		DetalleGestion:       detalleGestion,
		FechaRetornoCorasur:  fechaRetornoCorasur,
		TipoDescuento:        tipoDescuento,
		Caducidad:            caducidad,
		TipoServicio:         tipoServicio,
		Servicio:             servicio,
		FechaRegistro:        fechaRegistro,
		IdContacto:           idContacto,
	}
	return obj
}

func CrearGestionNegativa(
	placa,
	cliente,
	contactoCliente,
	activo string,
	idEstado int64,
	resultadoContacto string,
	fechaRetomarContacto,
	atendidoGestion,
	indicacionSugerida,
	detalleGestion,
	fechaRetornoCorasur,
	tipoDescuento,
	caducidad,
	tipoServicio,
	servicio,
	fechaRegistro string,
	idContacto int64) *GestionNegativa {
	obj := NuevaGestionNegativa(
		placa,
		cliente,
		contactoCliente,
		activo,
		idEstado,
		resultadoContacto,
		fechaRetomarContacto,
		atendidoGestion,
		indicacionSugerida,
		detalleGestion,
		fechaRetornoCorasur,
		tipoDescuento,
		caducidad,
		tipoServicio,
		servicio,
		fechaRegistro,
		idContacto)
	obj.Guardar()
	return obj
}

func (this *GestionNegativa) Guardar() {
	if this.IdGestionNegativa == 0 {
		this.insertar()
	}
}

func (this *GestionNegativa) insertar() {
	sql := `INSERT x_tagestionnegativa SET placa=?,
													cliente=?,
													contactoCliente=?,
													activo=?,
													idEstado=?,
													resultadoContacto=?,
													fechaRetomarContacto=?,
													atendidoGestion=?,
													indicacionSugerida=?,
													detalleGestion=?,
													fechaRetornoCorasur=?,
													tipoDescuento=?,
													caducidad=?,
													tipoServicio=?,
													servicio=?,
													fechaRegistro=?,
													idContacto=?`
	fechaRC, _ := time.Parse(time.RFC3339, this.FechaRetomarContacto)
	fechaRC2, _ := time.Parse(time.RFC3339, this.FechaRetomarContacto)
	result, err := Exec(sql,
		this.Placa,
		this.Cliente,
		this.ContactoCliente,
		this.Activo,
		this.IdEstado,
		this.ResultadoContacto,
		fechaRC,
		this.AtendidoGestion,
		this.IndicacionSugerida,
		this.DetalleGestion,
		fechaRC2,
		this.TipoDescuento,
		this.Caducidad,
		this.TipoServicio,
		this.Servicio,
		this.FechaRegistro,
		this.IdContacto)
	this.IdGestionNegativa, err = result.LastInsertId()
	if err != nil {
		println("Error:", err.Error())
	} else {
		println("LastInsertId:", this.IdGestionNegativa)
	}
}
