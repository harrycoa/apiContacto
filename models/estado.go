package models

type Estado struct {
	IdEstado int64  `json:"idEstado"`
	Eclase   string `json:"eClase"`
	Estado   string `json:"estado"`
}

type Estados []Estado

const estadoEsquema string = `CREATE TABLE x_taEstado (
	idEstado int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	eclase varchar(100) NOT NULL, 
	estado varchar(100) NOT NULL,  
	PRIMARY KEY (idEstado)
  );`

// Constructores
func NuevoEstado(eclase, estado string) *Estado {
	objestado := &Estado{Eclase: eclase, Estado: estado}
	return objestado
}

func CrearEstado(eclase, estado string) *Estado {
	objestado := NuevoEstado(eclase, estado)
	objestado.Guardar()
	return objestado
}

func ListarEstado(id int) *Estado {
	objestado := NuevoEstado("", "")
	sql := "SELECT idEstado, eclase, estado FROM x_taestado where idEstado=?"
	rows, _ := Query(sql, id)
	for rows.Next() {
		rows.Scan(&objestado.IdEstado, &objestado.Eclase, &objestado.Estado)
	}
	return objestado
}
func ListarEstados() Estados {

	sql := "SELECT idEstado, eclase, estado FROM x_taestado"
	listado := Estados{}
	rows, _ := Query(sql)

	for rows.Next() {
		obj := Estado{}
		rows.Scan(&obj.IdEstado, &obj.Eclase, &obj.Estado)
		listado = append(listado, obj)
	}
	return listado
}

func ListarEstadosParametro(eclase string) Estados {
	sql := "SELECT idEstado, eclase, estado FROM x_taestado where eclase=?"
	listado := Estados{}
	rows, _ := Query(sql, eclase)
	for rows.Next() {
		obj := Estado{}
		rows.Scan(&obj.IdEstado, &obj.Eclase, &obj.Estado)
		listado = append(listado, obj)
	}
	return listado
}

func (this *Estado) Guardar() {
	if this.IdEstado == 0 {
		this.insertar()
	} else {
		this.actualizar()
	}
}

func (this *Estado) insertar() {
	sql := "INSERT x_taestado SET eclase=?, estado=?"
	result, _ := Exec(sql, this.Eclase, this.Estado)
	this.IdEstado, _ = result.LastInsertId()
}

func (this *Estado) actualizar() {
	sql := "UPDATE x_taestado SET eclase=?, estado=?"
	Exec(sql, this.Eclase, this.Estado)
}

func (this *Estado) Delete() {
	sql := "DELETE FROM x_taestado WHERE idEstado=?"
	Exec(sql, this.IdEstado)
}
