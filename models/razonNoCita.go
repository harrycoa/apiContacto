package models

type RazonNoCita struct {
	IdRazonNoCita int64  `json:"idRazonNoCita"`
	RazonNoCita   string `json:"razonNoCita"`
	IdMotivo      int64
}

const razonNoCitaEsquema string = `CREATE TABLE x_tarazonnocita (
	idRazonNoCita int(11) NOT NULL AUTO_INCREMENT,
	razonNoCita varchar(200) DEFAULT NULL,
	idMotivo int(11) DEFAULT NULL,
	PRIMARY KEY (idRazonNoCita)
  );`

type RazonNoCitas []RazonNoCita

// constructores
func ListarRazonNoCitas() RazonNoCitas {
	sql := "SELECT idRazonNoCita, razonNoCita, idMotivo FROM x_tarazonnocita"
	listado := RazonNoCitas{}
	rows, _ := Query(sql)
	for rows.Next() {
		obj := RazonNoCita{}
		rows.Scan(&obj.IdRazonNoCita, &obj.RazonNoCita, &obj.IdMotivo)
		listado = append(listado, obj)
	}
	return listado
}

func ListarRazonNoCita(idMotivo string) RazonNoCitas {
	sql := "SELECT idRazonNoCita, razonNoCita, idMotivo FROM x_tarazonnocita WHERE idMotivo=?"
	listado := RazonNoCitas{}
	rows, _ := Query(sql, idMotivo)
	for rows.Next() {
		obj := RazonNoCita{}
		rows.Scan(&obj.IdRazonNoCita, &obj.RazonNoCita, &obj.IdMotivo)
		listado = append(listado, obj)
	}
	return listado
}
