package auth

// Response respuesta de login controller
type Response struct {
	Mensaje string `json:"mensaje"`
	Key     string `json:"key"`
}
