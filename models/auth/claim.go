package auth

import (
	user "../../models/user"
	jwt "github.com/dgrijalva/jwt-go"
)

// Claim clase claim
type Claim struct {
	user.TokenUser `json:"user"`
	jwt.StandardClaims
}
