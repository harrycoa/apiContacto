package models

type Respuesta struct {
	IdRespuesta int64  `json:"idRespuesta"`
	Rclase      string `json:"rclase"`
	Respuesta   string `json:"respuesta"`
}

const respuestaEsquema string = `CREATE TABLE x_taRespuesta (
	idRespuesta int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	rclase varchar(100) NOT NULL, 
	respuesta varchar(100) NOT NULL	
  );`

type Respuestas []Respuesta

// Constructores
func NuevaRespuesta(rclase, respuesta string) *Respuesta {
	objrespuesta := &Respuesta{Rclase: rclase, Respuesta: respuesta}
	return objrespuesta
}

func CrearRespuesta(rclase, respuesta string) *Respuesta {
	objrespuesta := NuevaRespuesta(rclase, respuesta)
	objrespuesta.Guardar()
	return objrespuesta
}

func ListarRespuesta(respuesta string) *Respuesta {
	objrespuesta := NuevaRespuesta("", "")
	sql := "SELECT idrespuesta, rclase, respuesta FROM x_tarespuesta where respuesta=?"
	rows, _ := Query(sql, respuesta)
	for rows.Next() {
		rows.Scan(&objrespuesta.IdRespuesta, &objrespuesta.Rclase, &objrespuesta.Respuesta)
	}
	return objrespuesta
}

func ListarRespuestas() Respuestas {

	sql := "SELECT idrespuesta, rclase, respuesta FROM x_tarespuesta"
	listado := Respuestas{}
	rows, _ := Query(sql)

	for rows.Next() {
		obj := Respuesta{}
		rows.Scan(&obj.IdRespuesta, &obj.Rclase, &obj.Respuesta)
		listado = append(listado, obj)
	}
	return listado
}

func ListarRespuestasParametro(rclase string) Respuestas {

	sql := "SELECT idRespuesta, rclase, respuesta FROM x_tarespuesta where rclase=?"
	listado := Respuestas{}
	rows, _ := Query(sql, rclase)
	for rows.Next() {
		obj := Respuesta{}
		rows.Scan(&obj.IdRespuesta, &obj.Rclase, &obj.Respuesta)
		listado = append(listado, obj)
	}
	return listado
}

func (this *Respuesta) Guardar() {
	if this.IdRespuesta == 0 {
		this.insertar()
	} else {
		this.actualizar()
	}
}

func (this *Respuesta) insertar() {
	sql := "INSERT x_tarespuesta SET rclase=?, respuesta=?"
	result, _ := Exec(sql, this.Rclase, this.Respuesta)
	this.IdRespuesta, _ = result.LastInsertId()
}

func (this *Respuesta) actualizar() {
	sql := "UPDATE x_tarespuesta SET rclase=?, respuesta=?"
	Exec(sql, this.Rclase, this.Respuesta)
}

func (this *Respuesta) Delete() {
	sql := "DELETE FROM x_tarespuesta WHERE idrespuesta=?"
	Exec(sql, this.IdRespuesta)
}
